﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class ClinicaController
    {
        public int Gravar(ClinicaViewModel cli)
        {
            clinica c = new clinica()
            {
                Bairro = cli.Bairro,
                Celular = cli.Celular,
                Cep = cli.Cep,
                Cidade = cli.Cidade,
                Cnpj = cli.Cnpj,
                Complemento = cli.Complemento,
                Email = cli.Email,
                Estado = cli.Estado,
                Logradouro = cli.Logradouro,
                Nome = cli.Nome,
                Numero = cli.Numero,
                Site = cli.Site,
                Telefone = cli.Telefone,
                Fax = cli.Fax,
                Cod = 0
            };
            return c.Gravar();
        }

        public int Editar(ClinicaViewModel cli)
        {
            clinica c = new clinica()
            {
                Bairro = cli.Bairro,
                Celular = cli.Celular,
                Cep = cli.Cep,
                Cidade = cli.Cidade,
                Cnpj = cli.Cnpj,
                Complemento = cli.Complemento,
                Email = cli.Email,
                Estado = cli.Estado,
                Logradouro = cli.Logradouro,
                Nome = cli.Nome,
                Numero = cli.Numero,
                Site = cli.Site,
                Telefone = cli.Telefone,
                Fax = cli.Fax,
                Cod = cli.Cod
            };
            return c.Editar();
        }

        public int Excluir(int cod)
        {
            return new clinica().Excluir(cod);
        }

        public ClinicaViewModel VerificarCadastro()
        {
            clinica c = new clinica().VerificarCadastro();
            if(c == null)
            {
                return null;
            }
            else
            {
                ClinicaViewModel vmCli = new ClinicaViewModel
                {
                    Bairro = c.Bairro,
                    Celular = c.Celular,
                    Cep = c.Cep,
                    Cidade = c.Cidade,
                    Cnpj = c.Cnpj,
                    Cod = c.Cod,
                    Complemento = c.Complemento,
                    Email = c.Email,
                    Estado = c.Estado,
                    Fax = c.Fax,
                    IE = c.Ie,
                    Logradouro = c.Logradouro,
                    Nome = c.Nome,
                    Numero = c.Numero,
                    Site = c.Site,
                    Telefone = c.Telefone
                };
                return vmCli;
            }
        }
    }
}