﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class EspecialidadeController
    {
        public int Gravar(EspecialidadeViewModel esp)
        {
            especialidade e = new especialidade()
            {
                Cod = 0,
                Descricao = esp.Descricao
            };
            return e.Gravar();            
        }

        public int Excluir(int cod)
        {
            return new especialidade().Excluir(cod);
        }

        public List<EspecialidadeViewModel> ObterEspecialidades()
        {
            especialidade e = new especialidade();
            var dados = e.ObterEspecialidades();
            return (from d in dados
                    orderby d.Cod
                    select new EspecialidadeViewModel
                    {
                        Cod = d.Cod,
                        Descricao = d.Descricao                        
                    }).ToList();
        }
    }
}