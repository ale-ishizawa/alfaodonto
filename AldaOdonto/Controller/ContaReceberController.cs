﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class ContaReceberController
    {

        public int BaixarContaReceber(int cod)
        {
            if(cod > 0)
            {
                return new conta_receber().BaixarContaReceber(cod);
            }
            else
            {
                return -10;
            }
        }

        public List<ContaReceberViewModel> ObterContas()
        {
            conta_receber c = new conta_receber();
            var dados = c.ObterContas();
            return (from d in dados
                    orderby d.DtVencto
                    select new ContaReceberViewModel
                    {
                        Cod = d.Cod,
                        DtPagto = d.DtPagto,
                        DtVencto = d.DtVencto,
                        FormaPagamento = d.FormaPagamento,
                        OrcamentoCod = d.OrcamentoCod,
                        PacienteCod = d.PacienteCod,
                        PacienteNome = d.paciente.Nome,
                        SaldoDevedor = d.SaldoDevedor,
                        Tipo = d.Tipo,
                        Valor = d.Valor
                    }).ToList();
        }
    }
}