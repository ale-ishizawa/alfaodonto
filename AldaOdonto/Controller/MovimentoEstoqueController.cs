﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class MovimentoEstoqueController
    {
        public int MovimentoEstoque(MovimentoEstoqueViewModel vmMov)
        {
            movimento_estoque mov = new movimento_estoque
            {
                Data = DateTime.Now,
                DentistaCod = vmMov.DentistaCod,
                Cod = vmMov.Cod,
                FuncionarioCod = vmMov.FuncionarioCod,
                Observacao = vmMov.Observacao,
                ProdutoCod = vmMov.ProdutoCod,
                Quantidade = vmMov.Quantidade,
                Tipo = vmMov.Tipo
            };
            return mov.MovimentoEstoque();
        }
    }
}