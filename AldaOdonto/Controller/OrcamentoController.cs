﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class OrcamentoController
    {
        public int Gravar(OrcamentoViewModel or)
        {
            orcamento o = new orcamento();
            List<itens_orcamento> _itens = new List<itens_orcamento>();
            foreach (ItensOrcamentoViewModel item in or.ItensOrcamento)
            {
                itens_orcamento i = new itens_orcamento();
                i.ProcedimentoCod = item.ProcedimentoCod;
                i.OrcamentoCod = item.OrcamentoCod;
                i.DenteRegiao = item.DenteRegiao;
                i.Quantidade = item.ItemQtd;
                i.Valor = item.ItemValor;
                _itens.Add(i);
            }
            
            o.DentistaCod = or.DentistaCod;
            o.DtInicio = or.DtInicio;
            o.DtFim = or.DtFim;
            o.FuncionarioCod = or.FuncionarioCod;
            o.itens_orcamento = _itens;
            o.Status = or.Status;
            o.Tipo = or.Tipo;
            o.PacienteCod = or.PacienteCod;
            o.ValorEntrada = or.ValorEntrada;
            o.ValorTotal = or.ValorTotal;
            o.Parcelas = or.Parcelas;
            o.Desconto = or.Desconto;
            return o.Gravar();
        }

        public int Excluir(int cod)
        {
            return new orcamento().Excluir(cod);
        }

        public int Editar(OrcamentoViewModel orc)
        {
            orcamento o = new orcamento();
            List<itens_orcamento> _itens = new List<itens_orcamento>();
            foreach (ItensOrcamentoViewModel item in orc.ItensOrcamento)
            {
                itens_orcamento i = new itens_orcamento();
                i.ProcedimentoCod = item.ProcedimentoCod;
                i.OrcamentoCod = item.OrcamentoCod;
                i.DenteRegiao = item.DenteRegiao;
                i.Quantidade = item.ItemQtd;
                i.Valor = item.ItemValor;
                _itens.Add(i);
            }
            o.DentistaCod = orc.DentistaCod;
            o.DtInicio = orc.DtInicio;
            o.DtFim = orc.DtFim;
            o.FuncionarioCod = orc.FuncionarioCod;
            o.itens_orcamento = _itens;
            o.Status = orc.Status;
            o.Tipo = orc.Tipo;
            o.PacienteCod = orc.PacienteCod;
            o.ValorEntrada = orc.ValorEntrada;
            o.ValorTotal = orc.ValorTotal;
            o.Parcelas = orc.Parcelas;
            o.Desconto = orc.Desconto;
            return o.Editar();
        }

        public List<OrcamentoViewModel> Obter()
        {
            orcamento o = new orcamento();
            var dados = o.Obter();         
            return (from d in dados
                    orderby d.Cod
                    select new OrcamentoViewModel
                    {
                        Cod = d.Cod,
                        DtInicio = d.DtInicio,
                        DentistaCod = d.DentistaCod,
                        DtFim = d.DtFim,
                        FuncionarioCod = d.FuncionarioCod, 
                        Status = d.Status,
                        PacienteCod = d.PacienteCod,
                        Tipo = d.Tipo,
                        NomeDentista = d.dentista.Nome,
                        NomePaciente = d.paciente.Nome,
                        NomeFuncionario = d.funcionario.Nome,
                        Parcelas = d.Parcelas,
                        ValorTotal = d.ValorTotal,
                        ValorEntrada = d.ValorEntrada,
                        Desconto = d.Desconto                     
                    }).ToList();
        }

        public List<OrcamentoViewModel> ObterPorPaciente(int pacienteCod)//Paciente Cod
        {
            orcamento o = new orcamento();
            var dados = o.ObterPorPaciente(pacienteCod);
            return (from d in dados
                    orderby d.Cod
                    select new OrcamentoViewModel
                    {
                        Cod = d.Cod,
                        DtInicio = d.DtInicio,
                        DentistaCod = d.DentistaCod,
                        DtFim = d.DtFim,
                        FuncionarioCod = d.FuncionarioCod,
                        Status = d.Status,
                        PacienteCod = d.PacienteCod,
                        Tipo = d.Tipo,
                        NomeDentista = d.dentista.Nome,
                        NomePaciente = d.paciente.Nome,
                        NomeFuncionario = d.funcionario.Nome,
                        Parcelas = d.Parcelas,
                        ValorTotal = d.ValorTotal,
                        ValorEntrada = d.ValorEntrada,
                        Desconto = d.Desconto
                    }).ToList();
        }

        public OrcamentoViewModel ObterPorCod(int codOrcamento)
        {
            orcamento o = new orcamento().ObterPorCod(codOrcamento);
            List<ItensOrcamentoViewModel> _itens = new List<ItensOrcamentoViewModel>();
            foreach (itens_orcamento item in o.itens_orcamento)
            {
                ItensOrcamentoViewModel i = new ItensOrcamentoViewModel
                {
                    DenteRegiao = item.DenteRegiao,
                    ItemQtd = item.Quantidade,
                    ItemValor = item.Valor,
                    OrcamentoCod = item.OrcamentoCod,
                    ProcedimentoCod = item.ProcedimentoCod
                };
                _itens.Add(i);
            }
            if(o == null)
            {
                return null;
            }
            else
            {
                OrcamentoViewModel vmOrc = new OrcamentoViewModel
                {
                    Cod = o.Cod,
                    DentistaCod = o.DentistaCod,
                    Desconto = o.Desconto,
                    DtInicio = o.DtInicio,
                    DtFim = o.DtFim,
                    FuncionarioCod = o.FuncionarioCod,
                    NomeDentista = o.dentista.Nome,
                    NomeFuncionario = o.funcionario.Nome,
                    NomePaciente = o.paciente.Nome,
                    PacienteCod = o.PacienteCod,
                    Parcelas = o.Parcelas,
                    Status = o.Status,
                    Tipo = o.Tipo,
                    ValorEntrada = o.ValorEntrada,
                    ValorTotal = o.ValorTotal,
                    ItensOrcamento = _itens
                };
                return vmOrc;
            }
                    
        }

        public int AprovarOrcamento(int cod)
        {
            return new orcamento().AprovarOrcamento(cod);
        }

    }
}