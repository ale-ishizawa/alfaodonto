﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class FuncionarioController
    {

        public int Gravar(FuncionarioViewModel fun)
        {
            funcionario f = new funcionario()
            {
                Bairro = fun.Bairro,
                Cep = fun.Cep,
                Cidade = fun.Cidade,
                ClinicaCod = fun.ClinicaCod,
                Complemento = fun.Complemento,
                Email = fun.Email,
                Estado = fun.Estado,
                Grupo = fun.Grupo,
                Logradouro = fun.Logradouro,
                Nome = fun.Nome,
                Numero = fun.Numero,
                Celular = fun.TelCelular,
                TelResidencia = fun.TelResidencia,
                Ativo = fun.Ativo,
                Login = fun.Login,
                CodFuncionario = fun.Cod,
                Senha = fun.Senha,
                DtNascto = fun.DtNascto                
            };
            
            return f.Gravar(f);
        }

        public int Editar(FuncionarioViewModel fun)
        {
            funcionario f = new funcionario()
            {
                Bairro = fun.Bairro,
                Cep = fun.Cep,
                Cidade = fun.Cidade,
                ClinicaCod = fun.ClinicaCod,
                Complemento = fun.Complemento,
                Email = fun.Email,
                Estado = fun.Estado,
                Grupo = fun.Grupo,
                Logradouro = fun.Logradouro,
                Nome = fun.Nome,
                Numero = fun.Numero,
                Celular = fun.TelCelular,
                TelResidencia = fun.TelResidencia
            };
            return f.Editar();
        }

        public int Excluir(int cod)
        {
            return new funcionario().Excluir(cod);
        }

        public List<FuncionarioViewModel> Pesquisar(string nome)
        {
            funcionario funcionario = new funcionario();
            return funcionario.Pesquisar(nome);
        }
    }
}