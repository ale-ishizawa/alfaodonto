﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class PerguntaController
    {
        public int Gravar(PerguntaViewModel per)
        {
            pergunta p = new pergunta()
            {
                Ativa = per.Ativa,
                Descricao = per.Descricao,
                Cod = per.Cod,
                Alerta = per.Alerta,
                AlertaDescricao = per.AlertaDescricao,
                Tipo = per.Tipo
            };
            return p.Gravar();
        }

        public int Editar(PerguntaViewModel per)
        {
            pergunta p = new pergunta()
            {
                Ativa = per.Ativa,
                Descricao = per.Descricao,
                Cod = per.Cod
            };
            return p.Gravar();
        }

        public int Excluir(int cod)
        {
            return new pergunta().Excluir(cod);
        }

        public List<PerguntaViewModel> ObterPerguntas()
        {
            pergunta p = new pergunta();
            var dados = p.ObterPerguntas();
            return (from d in dados
                    orderby d.Cod
                    select new PerguntaViewModel
                    {
                        Cod = d.Cod,
                        Ativa = d.Ativa,
                        Alerta = d.Alerta,
                        AlertaDescricao = d.AlertaDescricao,
                        AlertaQuando = "",
                        Descricao = d.Descricao,
                        Tipo = d.Tipo,
                    }).ToList();
        }

        public PerguntaViewModel ObterPorCod(int cod)
        {
            pergunta p = new pergunta().ObterPorCod(cod);
            if(p == null)
            {
                return null;
            }
            else
            {
                PerguntaViewModel vmPerg = new PerguntaViewModel
                {
                    Alerta = p.Alerta,
                    AlertaDescricao = p.AlertaDescricao,
                    AlertaQuando = "",
                    Ativa = p.Ativa,
                    Cod = p.Cod,
                    Descricao = p.Descricao,
                    Tipo = p.Tipo
                };
                return vmPerg;
            }
        }
    }
}