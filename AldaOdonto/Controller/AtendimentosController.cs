﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class AtendimentosController
    {
        public int FinalizarAtendimento(AtendimentoViewModel vmAt)
        {
            atendimentos a = new atendimentos
            {
                Data = vmAt.Data,
                Descricao = vmAt.Descricao
            };
            return a.FinalizarAtendimento();
        }
    }
}