﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class AgendamentoController
    {
        public int Gravar(AgendamentoViewModel ag, List<ItensOrcamentoViewModel> _itensOrcamento)
        {
           
            agendamento a = new agendamento();
            a.Cod = ag.Cod;
            a.Data = ag.Data;
            a.dentista.CodDentista = ag.DentistaCod;
            a.funcionario.CodFuncionario = ag.FuncionarioCod;
            a.paciente.CodPaciente = ag.PacienteCod;
            a.Observacao = ag.Observacao;
            a.TempoConsulta = ag.Tempo;
            a.Tipo = ag.Tipo;
            List<itens_orcamento> _itens = new List<itens_orcamento>();
            foreach (ItensOrcamentoViewModel item in _itensOrcamento)
            {
                itens_orcamento i = new itens_orcamento
                {
                    DenteRegiao = item.DenteRegiao,
                    OrcamentoCod = item.OrcamentoCod,
                    ProcedimentoCod = item.ProcedimentoCod,
                    Quantidade = item.ItemQtd,
                    Valor = item.ItemValor
                };
                _itens.Add(i);
            }
            return a.Gravar(_itens);
        }

        public int Excluir(int cod)
        {
            return new agendamento().Excluir(cod);
        }

        public int Editar(AgendamentoViewModel ag)
        {
            agendamento a = new agendamento
            {
                
                Cod = 0,
                Data = ag.Data,
                Observacao = ag.Observacao,
               // paciente = p,
                //funcionario = f,
                TempoConsulta = ag.Tempo,
                Tipo = ag.Tipo,
            };
            return a.Editar();
        }

        //Só para exibir os agendamentos do paciente no perfil dele, não terá ações
        public List<AgendamentoViewModel> ObterPorCod(int cod)
        {
            agendamento a = new agendamento();
            var dados = a.ObterPorCod(cod);
            return (from d in dados
                    orderby d.Data
                    select new AgendamentoViewModel
                    {
                        Cod = d.Cod,
                        Data = d.Data,                         
                    }).ToList();
        } 

        public List<AgendamentoViewModel> Obter()
        {
            agendamento a = new agendamento();
            var dados = a.Obter();
            return (from d in dados
                    select new AgendamentoViewModel
                    {
                        Cod = d.Cod,
                        Data = d.Data,
                        DentistaCod = d.DentistaCod,
                        FuncionarioCod = d.FuncionarioCod,
                        Observacao = d.Observacao,
                        PacienteCod = d.PacienteCod,
                        Tempo = d.TempoConsulta,
                        Tipo = d.Tipo
                    }).ToList();
        }
    }
}