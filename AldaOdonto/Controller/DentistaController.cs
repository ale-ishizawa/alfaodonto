﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class DentistaController
    {

        public int Gravar(DentistaViewModel den)
        {
            dentista d = new dentista()
            {
                Bairro = den.Bairro,
                Cep = den.Cep,
                Cidade = den.Cidade,
                ClinicaCod = den.ClinicaCod,
                Comissao = den.Comissao,
                Complemento = den.Complemento,
                Cpf = den.Cpf,
                Cro = den.Cro,
                DtNascto = den.DtNascto,
                Email = den.Email,
                Estado = den.Estado,
                Logradouro = den.Logradouro,
                Nome = den.Nome,
                Numero = den.Numero,
                Rg = den.Rg,
                SiglaConselho = den.SiglaConselho,
                Celular = den.TelCelular,
                TelResidencia = den.TelResidencia,
                TempoConsulta = den.TempoConsulta,
                UfConselho = den.UfConselho,
                Ativo = den.Ativo,
                Login = den.Login,
                Senha = den.Senha
            };
             return d.Gravar(d);
        }

        public string[] Pesquisar(string nome)
        {
            return new dentista().Pesquisar(nome);
            //dentista d = new dentista();
            //var dados = d.Pesquisar(nome);
            //return (from dentista in dados
            //        orderby dentista.Nome
            //        select new DentistaViewModel
            //        {
            //            Email = dentista.Email,
            //            Cod = dentista.Cod,
            //            Cro = dentista.Cro,
            //            Nome = dentista.Nome

            //        }).ToList();
        }
        public int Editar(DentistaViewModel den)
        {
            dentista d = new dentista()
            {
                Bairro = den.Bairro,
                Cep = den.Cep,
                Cidade = den.Cidade,
                ClinicaCod = den.ClinicaCod,
                CodDentista = den.Cod,
                Comissao = den.Comissao,
                Complemento = den.Complemento,
                Cpf = den.Cpf,
                Cro = den.Cro,
                DtNascto = den.DtNascto,
                Email = den.Email,
                Estado = den.Estado,
                Logradouro = den.Logradouro,
                Nome = den.Nome,
                Numero = den.Numero,
                Rg = den.Rg,
                SiglaConselho = den.SiglaConselho,
                Celular = den.TelCelular,
                TelResidencia = den.TelResidencia,
                TempoConsulta = den.TempoConsulta,
                UfConselho = den.UfConselho

            };
            return d.Editar();
        }

        public int Excluir(int cod)
        {
            return new dentista().Excluir(cod);
        }
    }
}