﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class ProcedimentoController
    {
        public int Gravar(ProcedimentoViewModel procVm)
        {
            procedimento p = new procedimento()
            {
                Cod = 0,
                Descricao = procVm.Descricao,
                EspecialidadeCod = procVm.EspecialidadeCod,
                Valor = procVm.Valor
            };
            return p.Gravar();
        }

        public int Editar(ProcedimentoViewModel procVm)
        {
            procedimento p = new procedimento
            {
                Cod = procVm.Cod,
                Valor = procVm.Valor,
                EspecialidadeCod = procVm.EspecialidadeCod

            };
            return p.Editar();
        }

        public List<ProcedimentoViewModel> ObterProcedimentos()
        {
            procedimento p = new procedimento();
            var dados = p.ObterProcedimentos();
            return (from d in dados
                    orderby d.especialidade.Descricao
                    select new ProcedimentoViewModel
                    {
                        Cod = d.Cod,
                        Descricao = d.Descricao,
                        EspecialidadeCod = d.EspecialidadeCod,
                        Valor = d.Valor,
                        EspecialidadeDescricao = d.especialidade.Descricao
                    }).ToList();        
        }

        public int Excluir(int cod)
        {
            return new procedimento().Excluir(cod);
        }

        public string[] Pesquisar(string nome)
        {
            return new procedimento().Pesquisar(nome);
        }

        public ProcedimentoViewModel ObterPorCod(int cod)
        {
            procedimento p = new procedimento().ObterPorCod(cod);
            if (p == null)
            {
                return null;
            }
            else
            {
                ProcedimentoViewModel vmPro = new ProcedimentoViewModel();
                vmPro.Cod = p.Cod;
                vmPro.Descricao = p.Descricao;
                vmPro.EspecialidadeCod = p.EspecialidadeCod;
                vmPro.Valor = p.Valor;
                return vmPro;
            }
        }
    }
}