﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class AnamneseController
    {

        public int Gravar(AnamneseViewModel ana)
        {
            anamnese a = new anamnese
            {
                Cod = 0,
                Nome = ana.Nome,
                
                
            };
            return a.Gravar();
        }

        public List<AnamneseViewModel> ObterAnamneses()
        {
            anamnese a = new anamnese();
            var dados = a.ObterAnamneses();
            return (from d in dados
                    orderby d.Cod
                    select new AnamneseViewModel
                    { 
                        Cod = d.Cod,
                        Nome = d.Nome
                    }).ToList();          

        }

        public AnamneseViewModel ObterPorCod(int cod)
        {
            anamnese a = new anamnese().ObterPorCod(cod);
            if(a == null)
            {
                return null;
            }
            else
            {
                AnamneseViewModel vmAna = new AnamneseViewModel();
                vmAna.Cod = a.Cod;
                vmAna.Nome = a.Nome;
                return vmAna;
            }
        }

        public int Excluir(int cod)
        {
            return new anamnese().Excluir(cod);
        }

        public int Editar(AnamneseViewModel vmAna)
        {
            anamnese a = new anamnese();
            a.Cod = vmAna.Cod;
            a.Nome = vmAna.Nome;
            return a.Editar();
        }
    }
}