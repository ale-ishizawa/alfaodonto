﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class ProdutoController
    {
        public int Gravar(ProdutoViewModel pro)
        {
            produto p = new produto();
            p.Cod = pro.Cod;
            p.Descricao = pro.Descricao;
            p.Quantidade = pro.Quantidade;
            p.QtdCritica = pro.QtdCritica;
            return p.Gravar();
        }

        public int Excluir(int cod)
        {
            return new produto().Excluir(cod);
        }

        public int BaixarEstoque(ProdutoViewModel proVM)
        {
            produto pro = new produto()
            {
                Cod = proVM.Cod,
                Quantidade = proVM.Quantidade
            };
            return pro.BaixarEstoque();
        }

        public List<ProdutoViewModel> ObterProdutos()
        {
            produto p = new produto();
            var dados = p.ObterProdutos();
            return (from d in dados
                    orderby d.Cod
                    select new ProdutoViewModel
                    {
                        Cod = d.Cod,
                        Descricao = d.Descricao,
                        QtdCritica = d.QtdCritica, 
                        Quantidade = d.Quantidade
                    }).ToList();
        }

        public ProdutoViewModel ObterPorCod(int cod)
        {
            produto p = new produto().ObterPorCod(cod);
            if(p == null)
            {
                return null;
            }
            else
            {
                ProdutoViewModel vmPro = new ProdutoViewModel();
                vmPro.Cod = p.Cod;
                vmPro.Descricao = p.Descricao;
                vmPro.QtdCritica = p.QtdCritica;
                vmPro.Quantidade = p.Quantidade;
                return vmPro;
            }

        }

        public int Editar(ProdutoViewModel vmPro)
        {
            produto p = new produto();
            p.Cod = vmPro.Cod;
            p.Descricao = vmPro.Descricao;
            p.Quantidade = vmPro.Quantidade;
            p.QtdCritica = vmPro.QtdCritica;
            return p.Editar();
        }
    }
}