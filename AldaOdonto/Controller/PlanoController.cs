﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class PlanoController
    {
        //public int Gravar(PlanoViewModel pla)
        //{
            //List<plano_procedimentos> _procedimentos = new List<plano_procedimentos>();
            //foreach (ProcedimentoViewModel pro in pla.PlanoProcedimentos)
            //{
            //    procedimento procedimento = new procedimento
            //    {
            //        Cod = pro.Cod,
            //        Descricao = pro.Descricao,
            //        Valor = pro.Valor,
            //        EspecialidadeCod = pro.EspecialidadeCod
            //    };
            //    _procedimentos.Add(procedimento);
            //} 
            //plano p = new plano()
            //{
            //    Cod = 0,
            //    Descricao = pla.Descricao,
            //    Valor = pla.Valor,
            //    Padrao = pla.Padrao,
            //    procedimento = _procedimentos
            //};
            //return p.Gravar();
        //}

        public int Editar(PlanoViewModel pla)
        {
            plano p = new plano()
            {
                Cod = pla.Cod,
                Descricao = pla.Descricao,
                Valor = pla.Valor,
                Padrao = pla.Padrao
            };
            return p.Editar();
        }

        public int Excluir(int cod)
        {
            return new plano().Excluir(cod);
        }
    }
}