﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Controller
{
    public class PacienteController
    {

        public string[] PesquisarPaciente(string nome)
        {
            return new paciente().PesquisarPaciente(nome);
            //List<paciente> _pacientes = new paciente().Pesquisar(nome);
            //return (from pp in _pacientes
            //            select new string
            //            {
            //                Cod = pp.Cod,
            //                Nome = pp.Nome,
            //                Cpf = pp.Cpf,
            //                Bairro = pp.Bairro,
            //                Cep = pp.Cep,
            //                Cidade = pp.Cidade,
            //                ClinicaCod = pp.ClinicaCod,
            //                Complemento = pp.Complemento,
            //                DiaVencto = pp.DiaVencto,
            //                DtCadastro = pp.DtCadastro,
            //                DtNascto = pp.DtNascto,
            //                Email = pp.Email,
            //                Estado = pp.Estado,
            //                EstadoCivil = pp.EstadoCivil,
            //                Logradouro = pp.Logradouro,
            //                Genero = pp.Genero,
            //                NomeRecado = pp.NomeRecado,
            //                OrgaoEmissor = pp.OrgaoEmissor,
            //                Numero = pp.Numero,
            //                PlanoCod = pp.PlanoCod,
            //                Rg = pp.Rg,
            //                TelCelular = pp.TelCelular,
            //                TelComercial = pp.TelComercial,
            //                TelRecado = pp.TelRecado,
            //                TelResidencia = pp.TelResidencia
            //            }).ToArray();
        }

        public PacienteViewModel Obter(int id)
        {
            throw new NotImplementedException();
        }

        public int Gravar(PacienteViewModel pac)
        {
            paciente p = new paciente()
            {
                Nome = pac.Nome,
                Cpf = pac.Cpf,
                Bairro = pac.Bairro,
                Cep = pac.Cep,
                Cidade = pac.Cidade,
                ClinicaCod = pac.ClinicaCod,
                Complemento = pac.Complemento,
                DiaVencto = pac.DiaVencto,
                DtCadastro = pac.DtCadastro,
                DtNascto = pac.DtNascto,
                Email = pac.Email,
                Estado = pac.Estado,
                EstadoCivil = pac.EstadoCivil,
                Logradouro = pac.Logradouro,
                Genero = pac.Genero,
                NomeRecado = pac.NomeRecado,
                OrgaoEmissor = pac.OrgaoEmissor,
                Numero = pac.Numero,
                PlanoCod = pac.PlanoCod,
                Rg = pac.Rg,
                Celular = pac.TelCelular,
                TelComercial = pac.TelComercial,
                TelRecado = pac.TelRecado,
                TelResidencia = pac.TelResidencia,
                Senha = pac.Senha,
                Ativo = pac.Ativo,
                Login = pac.Login,
                CodPaciente = pac.Cod
            };
            
            return p.Gravar(p);
        }

        public int Editar(PacienteViewModel pacVm)
        {
            paciente p = new paciente()
            {
                Nome = pacVm.Nome,
                Cpf = pacVm.Cpf,
                Bairro = pacVm.Bairro,
                Cep = pacVm.Cep,
                Cidade = pacVm.Cidade,
                ClinicaCod = pacVm.ClinicaCod,
                Complemento = pacVm.Complemento,
                DiaVencto = pacVm.DiaVencto,
                DtCadastro = pacVm.DtCadastro,
                DtNascto = pacVm.DtNascto,
                Email = pacVm.Email,
                Estado = pacVm.Estado,
                EstadoCivil = pacVm.EstadoCivil,
                Logradouro = pacVm.Logradouro,
                Genero = pacVm.Genero,
                NomeRecado = pacVm.NomeRecado,
                OrgaoEmissor = pacVm.OrgaoEmissor,
                Numero = pacVm.Numero,
                PlanoCod = pacVm.PlanoCod,
                Rg = pacVm.Rg,
                Celular = pacVm.TelCelular,
                TelComercial = pacVm.TelComercial,
                TelRecado = pacVm.TelRecado,
                TelResidencia = pacVm.TelResidencia
            };
            return p.Editar();
        }

        public int Excluir(int cod)
        {
            return new paciente().Excluir(cod);
        }
    }
}