﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class ProcedimentoDAO
    {
        internal int Gravar(procedimento p)
        {
           using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
            {
                try
                {
                    contexto.procedimento.Add(p);
                    return contexto.SaveChanges();
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Erro ao tentar gravar um novo procedimento. ", ex.InnerException);
#endif
                    return -1;
                }
            }
        }

        internal int Editar(procedimento p)
        {
            using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
            {
                try
                {
                    procedimento pAtual = contexto.procedimento.Find(p);
                    pAtual.Cod = p.Cod;
                    pAtual.Descricao = p.Descricao;
                    pAtual.Valor = p.Valor;
                    pAtual.EspecialidadeCod = p.EspecialidadeCod;
                    contexto.Entry(pAtual).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                    
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Erro ao tentar editar um procedimento. ", ex.InnerException);
#endif
                    return -1;
                }
            }
        }

        internal procedimento ObterPorCod(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.procedimento.Find(cod);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar Obter um procedimento pelo código. ", ex.InnerException);
#endif
                return null;
            }
        }

        internal List<procedimento> ObterProcedimentos()
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.procedimento.Include("especialidade").ToList();                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar obter procedimentos. " + ex.InnerException);
#endif
                return null;
            }
        }

        internal string[] Pesquisar(string nome)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    List<procedimento> _procedimentos;
                    _procedimentos = contexto.procedimento.Where(p => p.Descricao.Contains(nome)).ToList();
                    List<string> procedimentos = new List<string>();
                    foreach (procedimento p in _procedimentos)
                    {
                        procedimentos.Add(string.Format("{0}-{1}", p.Descricao, p.Cod));
                    }
                    return procedimentos.ToArray();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar obter procedimentos. " + ex.InnerException);
#endif
                return null;
            }
        }

        internal int Excluir(int cod)
        {
            using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
            {
                try
                {
                    procedimento p = contexto.procedimento.Find(cod);
                    itens_orcamento item = contexto.itens_orcamento.Where(x => x.ProcedimentoCod == cod).FirstOrDefault();
                    if(item == null)
                    {
                        contexto.procedimento.Remove(p);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //mensagem para o usuário
                        return -5;
                    }
                    
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Erro ao tentar excluir um procedimento. ", ex.InnerException);
#endif
                    return -1;
                }
            }
        }
    }
}