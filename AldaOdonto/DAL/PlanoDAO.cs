﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    internal class PlanoDAO
    {
        internal int Gravar(plano plano)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    if(plano.procedimento != null && plano.procedimento.Count() > 0)
                    {
                        //Para cada objeto procedimento dentro do plano, precisamos
                        //informar ao Entity que esse objeto já existe no contexto e por
                        //isso não deve ser persistido novamente.
                        foreach (procedimento pro in plano.procedimento)
                        {
                            if (contexto.Entry(pro).State == System.Data.Entity.EntityState.Detached)
                                contexto.procedimento.Attach(pro);
                        }
                        contexto.plano.Add(plano);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        contexto.plano.Add(plano);
                        return contexto.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar gravar um novo plano. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Editar(plano plaAlterado)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    plano plaAtual = contexto.plano.Find(plaAlterado.Cod);
                    plaAtual.Descricao = plaAlterado.Descricao;
                    plaAtual.Padrao = plaAlterado.Padrao;
                    plaAtual.Valor = plaAlterado.Valor;

                    var procDeletados = (from p in plaAtual.procedimento
                                         where !(from p1 in plaAlterado.procedimento
                                                 select p1.Cod).Contains(p.Cod)
                                         select p).ToList();

                    var procAdicionados = (from p in plaAlterado.procedimento
                                           where !(from p1 in plaAtual.procedimento
                                                   select p1.Cod).Contains(p.Cod)
                                           select p).ToList();

                    procDeletados.ForEach(p => plaAtual.procedimento.Remove(p));
                    foreach (procedimento pro in procAdicionados)
                    {
                        if (contexto.Entry(pro).State == System.Data.Entity.EntityState.Detached)
                            contexto.procedimento.Attach(pro);
                        plaAtual.procedimento.Add(pro);
                    }
                    contexto.Entry(plaAtual).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar um plano. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    plano p = contexto.plano.Find(cod);
                    if (p.Padrao == "S")
                    {
                        //MSG USUÁRIO
                        return -5;
                    }
                    int qtdPacientes = contexto.paciente.Where(x => x.PlanoCod == cod).Count();
                    if(qtdPacientes > 0)
                    {
                        //MSG USUÁRIO
                        return -55;
                    }
                    else
                    {
                        /*
                        Criando um lista auxiliar de procedimentos
                        que estão atreladas ao objeto plano a ser excluído.
                        Isso é necessário para não dar erro de integridade referencial
                        na tabela muitos para muitos.
                        */
                        List<procedimento> auxProcedimentos = null;
                        if(p.plano_procedimentos != null)
                        {
                            auxProcedimentos = p.procedimento.ToList();
                            //Removendo os objetos procedimentos da lista
                            //atrelada ao objeto plano. A lista de procedimentos
                            //ficará vazia.
                            auxProcedimentos.ForEach(x => p.procedimento.Remove(x));
                        }

                        /*Quando o EF percebe que ele tem uma lista vazia atrelada
                        a um muitos para muitos ele deleta as referências da tabela MM relacionada
                        antes de deletar a atividade.
                        */
                        contexto.plano.Remove(p);
                        return contexto.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar excluir um plano. ", ex.InnerException);
#endif
                return -1;
            }
        }
    }
}