﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class AnamneseDAO
    {
        internal int Gravar(anamnese anamnese)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.anamnese.Add(anamnese);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao gravar uma nova anamnese. " + ex.Message);
#endif
                return -1;
            }
        }

        internal anamnese ObterPorCod(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.anamnese.Where(x => x.Cod == cod).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter anamnese por Código. " + ex.Message);
#endif
                return null;
            }
        }

        internal int Editar(anamnese anamnese)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.Entry(anamnese).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao editar uma anamnese: " + ex.Message);
#endif
                return -1;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    //Se algum paciente já respondeu alguma pergunta da anamnese, não será possível excluir
                    paciente_respostas pacRes = contexto.paciente_respostas.Where(x => x.AnamneseCod == cod).FirstOrDefault();
                    if(pacRes == null)
                    {
                        anamnese a = contexto.anamnese.Where(x => x.Cod == cod).FirstOrDefault();
                        contexto.anamnese.Remove(a);
                        return contexto.SaveChanges();
                    }
                    else
                    {//Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao excluir uma anamnese. " + ex.Message);
#endif
                return -1;
            }
        }

        internal List<anamnese> ObterAnamneses()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.anamnese.ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }


}