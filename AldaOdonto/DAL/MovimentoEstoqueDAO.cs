﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    public class MovimentoEstoqueDAO
    {
        internal int MovimentoEstoque(movimento_estoque mov)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.movimento_estoque.Add(mov);
                    produto p = contexto.produto.Find(mov.ProdutoCod);
                    if(p != null)
                    {
                        if(mov.Tipo == 1)
                        {
                            //É entrada
                            p.Quantidade += mov.Quantidade;
                            contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            //É retirada
                            p.Quantidade -= mov.Quantidade;
                            contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        }                        
                    }
                    else
                    {
                        return -5;
                    }
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao movimentar estoque de um produto. " + ex.Message);
#endif
                return -1;
            }
        }
       
    }
}