﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class AtendimentosDAO
    {
        internal int FinalizarAtendimento(atendimentos atendimentos)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    //Update na tabela atendimentos no agendamento finalizado
                    atendimentos a = contexto.atendimentos.Find(atendimentos.AgendamentoCod);
                    a.Data = atendimentos.Data;
                    a.Descricao = atendimentos.Descricao;
                    contexto.Entry(a).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
                
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao finalizar o atendimento: " + ex.Message);
#endif
                return -1;
            }
        }
    }
}