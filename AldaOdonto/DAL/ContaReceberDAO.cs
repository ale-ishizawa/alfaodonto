﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    public class ContaReceberDAO
    {
        internal int BaixarContaReceber(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    conta_receber c = contexto.conta_receber.Find(cod);
                    if(c != null)
                    {
                        c.DtPagto = DateTime.Now;
                        contexto.Entry(c).State = System.Data.Entity.EntityState.Modified;
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //MSG USUÁRIO
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao gravar baixar conta a receber. " + ex.Message);
#endif
                return -1;
            }
        }

        internal List<conta_receber> ObterContas()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.conta_receber.Where(c => c.DtPagto == null || c.DtPagto.ToString() == "").ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter contas a receber. " + ex.Message);
#endif
                return null;
            }
        }
    }
}