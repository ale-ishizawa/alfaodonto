﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    internal class ClinicaDAO
    {
        internal int Gravar(clinica clinica)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.clinica.Add(clinica);
                    return contexto.SaveChanges();
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar gravar uma nova clínica. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Editar(clinica cli)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    clinica c = contexto.clinica.Find(cli.Cod);
                    c.Bairro = cli.Bairro;
                    c.Celular = cli.Celular;
                    c.Cep = cli.Cep;
                    c.Cidade = cli.Cidade;
                    c.Cnpj = cli.Cnpj;
                    c.Complemento = cli.Complemento;
                    c.Email = cli.Email;
                    c.Estado = cli.Estado;
                    c.Fax = cli.Fax;
                    c.Logradouro = cli.Logradouro;
                    c.Nome = cli.Nome;
                    c.Numero = cli.Numero;
                    c.Site = cli.Site;
                    c.Telefone = cli.Telefone;
                    contexto.Entry(c).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar uma clínica. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal clinica VerificarCadastro()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.clinica.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter o cadastro da clínica. " + ex.Message);
#endif
                return null;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    clinica c = contexto.clinica.Find(cod);
                    contexto.clinica.Remove(c);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar excluir uma clínica. ", ex.InnerException);
#endif
                return -1;
            }
        }
    }
}