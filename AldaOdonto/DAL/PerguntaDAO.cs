﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class PerguntaDAO
    {
        internal int Gravar(pergunta pergunta)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.pergunta.Add(pergunta);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar gravar uma nova pergunta. " + ex.Message);
#endif
                return -1;
            }
        }

        internal int Editar(pergunta per)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    pergunta p = contexto.pergunta.Find(per.Cod);
                    p.Ativa = per.Ativa;
                    p.Descricao = per.Descricao;
                    contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar uma pergunta. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal pergunta ObterPorCod(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.pergunta.Where(x => x.Cod == cod).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter pergunta por Código. " + ex.Message);
#endif
                return null;
            }
        }

        internal List<pergunta> ObterPerguntas()
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.pergunta.ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter as perguntas: " + ex.Message);
#endif
                return null;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    pergunta p = contexto.pergunta.Find(cod);
                    contexto.pergunta.Remove(p);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar excluir uma pergunta. ", ex.InnerException);
#endif
                return -1;
            }
        }
    }
}