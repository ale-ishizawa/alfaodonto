﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;
using AldaOdonto.ViewModel;

namespace AldaOdonto.DAL
{
    internal class FuncionarioDAO
    {
        internal int Gravar(funcionario fun)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {                           
                    contexto.funcionario.Add(fun);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar gravar um novo funcionário. " + ex.Message);
#endif
                return -1;
            }
        }

        internal int Editar(funcionario fun)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    funcionario f = contexto.Find(fun.CodFuncionario);
                    f.Bairro = fun.Bairro;
                    f.Cep = fun.Cep;
                    f.Cidade = fun.Cidade;
                    f.ClinicaCod = fun.ClinicaCod;
                    f.CodFuncionario = fun.CodFuncionario;
                    f.Complemento = fun.Complemento;
                    f.Email = fun.Email;
                    f.Estado = fun.Estado;
                    f.Grupo = fun.Grupo;
                    f.Logradouro = fun.Logradouro;
                    f.Nome = fun.Nome;
                    f.Numero = fun.Numero;
                    f.Celular = fun.Celular;
                    f.TelResidencia = fun.TelResidencia;
                    contexto.Entry(f).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar um funcionário. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    usuario u = contexto.usuario.Find(cod);
                    if (u != null)
                    {
                        u.Ativo = 2;
                        contexto.Entry(u).State = System.Data.Entity.EntityState.Modified;
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar um funcionário. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal List<FuncionarioViewModel> Pesquisar(string nome)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    var dados = (from c in contexto.funcionario
                                 where c.Nome.Contains(nome) && c.Ativo == 1
                                 orderby c.Nome
                                 select new FuncionarioViewModel
                                 {
                                     Email = c.Email,
                                     Cod = c.CodFuncionario,
                                     Nome = c.Nome

                                 }).ToList();
                    return dados;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar pesquisar um funcionário. ", ex.InnerException);
#endif
                return null;
            }
        }
    }
}