﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    internal class EspecialidadeDAO
    {
        internal int Gravar(especialidade e)
        {
            using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
            {
                try
                {
                    contexto.especialidade.Add(e);
                    return contexto.SaveChanges();
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Erro ao tentar gravar uma nova especialidade. "+ ex.Message);
#endif
                    return -1;
                }
            }
        }

        internal int Excluir(int cod)
        {

            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    especialidade e = contexto.especialidade.Find(cod);
                    procedimento p = contexto.procedimento.Where(x => x.EspecialidadeCod == cod).FirstOrDefault();
                    if (p == null)
                    {
                        contexto.especialidade.Remove(e);
                        return contexto.SaveChanges();
                    }

                    else
                    {
                        //Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar excluir uma especialidade: " + ex.Message);
#endif
                return -1;
            }
            
        }
        
        internal List<especialidade> ObterEspecialidades()
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.especialidade.OrderBy(x => x.Cod).ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter as especialidades cadastradas: " + ex.InnerException);
#endif
                return null;
            }
        }       
    }
}