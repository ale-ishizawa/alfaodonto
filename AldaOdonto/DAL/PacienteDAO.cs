﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    internal class PacienteDAO
    {
        internal int Gravar(paciente pac)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.paciente.Add(pac);
                    if(pac.PlanoCod > 0)
                    {
                        //Significa que o paciente comprou um plano, fazer o registro da venda
                        //gerando contas a receber

                    }
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar gravar um novo paciente. " + ex.Message);
#endif
                return -1;
            }
            
        }

        internal string[] PesquisarPaciente(string nome)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    List<paciente> _pacientes;
                    //VERIFICAR O USO DO CONTAINS
                    _pacientes = contexto.paciente.Where(p => p.Nome.Contains(nome)).ToList();// && p.usuario.Ativo == 1).ToList();
                    List<string> pacientes = new List<string>();
                    foreach (paciente p in _pacientes)
                    {
                        pacientes.Add(string.Format("{0}-{1}", p.Nome, p.CodPaciente));
                    }
                    return pacientes.ToArray();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar obter um paciente. " + ex.InnerException);
#endif
                return null;
            }
        }

        internal int Editar(paciente pac)
        {
         try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    paciente p = contexto.paciente.Find(pac.CodPaciente);
                    p.Nome = pac.Nome;
                    p.Cpf = pac.Cpf;
                    p.Bairro = pac.Bairro;
                    p.Cep = pac.Cep;
                    p.Cidade = pac.Cidade;
                    p.ClinicaCod = pac.ClinicaCod;
                    p.Complemento = pac.Complemento;
                    p.DiaVencto = pac.DiaVencto;
                    p.DtCadastro = pac.DtCadastro;
                    p.DtNascto = pac.DtNascto;
                    p.Email = pac.Email;
                    p.Estado = pac.Estado;
                    p.EstadoCivil = pac.EstadoCivil;
                    p.Logradouro = pac.Logradouro;
                    p.Genero = pac.Genero;
                    p.NomeRecado = pac.NomeRecado;
                    p.OrgaoEmissor = pac.OrgaoEmissor;
                    p.Numero = pac.Numero;
                    p.PlanoCod = pac.PlanoCod;
                    p.Rg = pac.Rg;
                    p.Celular = pac.Celular;
                    p.TelComercial = pac.TelComercial;
                    p.TelRecado = pac.TelRecado;
                    p.TelResidencia = pac.TelResidencia;
                    contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar um paciente. " + ex.InnerException);
#endif
                return -1;
            }
            
        }

        internal int Excluir(int cod)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    paciente p = contexto.paciente.Where(x => x.CodPaciente == cod).FirstOrDefault();
                    if (p != null)
                    {
                        p.Ativo = 2;
                        contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        return contexto.SaveChanges();
                    }

                    else
                    {
                        //Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar excluir um novo paciente. " + ex.InnerException);
#endif
                return -1;
            }
            
        }
    }
}