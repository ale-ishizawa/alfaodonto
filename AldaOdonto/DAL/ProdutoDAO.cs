﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    public class ProdutoDAO
    {

        internal int Gravar(produto pro)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.produto.Add(pro);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao gravar um novo produto. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    produto pro = contexto.produto.Find(cod);
                    movimento_estoque mov = contexto.movimento_estoque.Where(x => x.ProdutoCod == cod).FirstOrDefault();
                    //Se o mov for != null então há registros com o produto, não pode ser excluído
                    if (mov == null)
                    {
                        contexto.produto.Remove(pro);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao excluir um produto. "+ ex.Message);
#endif
                return -1;
            }
        }

        internal int Editar(produto produto)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.Entry(produto).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao editar um produto: "+ ex.Message);
#endif
                return -1;
            }
        }

        internal List<produto> ObterProdutos()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.produto.OrderBy(x => x.Cod).ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter os produtos cadastrados: " + ex.Message);
#endif
                return null;
            }
        }

        internal int BaixarEstoque(produto pro)
        {

            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    produto p = contexto.produto.Find(pro.Cod);
                    p.Quantidade -= pro.Quantidade;
                    contexto.Entry(p).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao baixar estoque de produto. ", ex.InnerException);
#endif
                return -1;
            }
                       
        }

        internal produto ObterPorCod(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.produto.Where(x => x.Cod == cod).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter produto por Código. " + ex.Message);
#endif
                return null;
            }
        }
    }
}