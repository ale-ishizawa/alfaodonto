﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class AgendamentoDAO
    {
        internal int Gravar(agendamento agendamento, List<itens_orcamento> _itensOrcamento)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    if(_itensOrcamento.Count == 0 || _itensOrcamento == null)
                    {
                        //Significa que é apenas uma nova consulta, não é um agendamento de um orçamento já aprovado
                        atendimentos a = new atendimentos
                        {
                            AgendamentoCod = agendamento.Cod
                        };
                        contexto.atendimentos.Attach(a);
                        contexto.agendamento.Add(agendamento);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //Significa que é um agendamento de um orçamento aprovado
                        //Inserir na tabela atendimentos MxM
                        contexto.agendamento.Add(agendamento);
                        foreach (itens_orcamento item in _itensOrcamento)
                        {
                            //Apenas registro o atendimento na tabela MxM, aqui não é a finalização do mesmo.
                            //Depois quando o Dentista realizar o agendamento, ele finaliza o atendimento, atualizando os 
                            //campos Descrição e Data.
                            atendimentos a = new atendimentos
                            {
                                AgendamentoCod = agendamento.Cod,
                                OrcamentoCod = item.OrcamentoCod,
                                ProcedimentoCod = item.ProcedimentoCod
                            };
                            if(contexto.Entry(a).State == System.Data.Entity.EntityState.Detached)
                                contexto.atendimentos.Attach(a);                           
                        }
                        return contexto.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao gravar um novo agendamento. ", ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    agendamento ag = contexto.agendamento.Find(cod);
                    List<atendimentos> atendimentos = contexto.atendimentos.Where(a => a.AgendamentoCod == cod && a.Descricao == "" || a.Descricao == null).ToList();
                    if(ag != null && atendimentos.Count > 0)
                    {
                        atendimentos.ForEach(x => ag.atendimentos.Remove(x));
                        contexto.agendamento.Remove(ag);
                        return contexto.SaveChanges();
                    }
                    if(ag != null && atendimentos == null || atendimentos.Count <= 0)
                    {
                        contexto.agendamento.Remove(ag);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //mensagem usuário
                        return -5;
                    }
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao excluir um agendamento. " + ex.Message);
#endif
                return -1;
            }
        }

        internal List<agendamento> ObterPorCod(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.agendamento.Where(a => a.paciente.CodPaciente == cod).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        internal List<agendamento> Obter()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    //Só obtenho agendamentos que ainda não foram feitos
                    return contexto.agendamento.Where(a => a.Data > DateTime.Now).ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao Obter todos agendamentos. " + ex.Message);
#endif
                return null;
            }
        }

        internal int Editar(agendamento agendamento)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.Entry(agendamento).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao editar um agendamento: " + ex.Message);
#endif
                return -1;
            }
        }
    }
}