﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AldaOdonto.Model;

namespace AldaOdonto.DAL
{
    public class OrcamentoDAO
    {
        internal int Gravar(orcamento orcamento)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    //Verifico se o novo orçamento possui objetos itens_orcamento associados a ele
                    if(orcamento.itens_orcamento != null && orcamento.itens_orcamento.Count() > 0)
                    {
                        //Para cada objeto itens_orcamento dentro do orcamento, preciso informar ao Entity
                        //que esse objeto já existe no contexto e por isso não deve ser persistido novamente.
                        foreach (itens_orcamento item in orcamento.itens_orcamento)
                        {
                            if (contexto.Entry(item).State == System.Data.Entity.EntityState.Detached)
                                contexto.itens_orcamento.Attach(item);
                        }                        
                    }
                    contexto.orcamento.Add(orcamento);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao gravar um novo orçamento. "+ ex.Message);
#endif
                return -1;
            }
        }

        internal int Editar(orcamento orcAlterado)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    orcamento orcAtual = contexto.orcamento.Find(orcAlterado.Cod);
                    orcAtual.DentistaCod = orcAlterado.DentistaCod;
                    orcAtual.DtFim = orcAlterado.DtFim;
                    orcAtual.DtInicio = orcAlterado.DtInicio;
                    orcAtual.FuncionarioCod = orcAlterado.FuncionarioCod;
                    orcAtual.PacienteCod = orcAlterado.PacienteCod;
                    orcAtual.Status = orcAlterado.Status;
                    orcAtual.Tipo = orcAlterado.Tipo;

                    var itensDeletados = (from i in orcAtual.itens_orcamento
                                         where !(from i1 in orcAlterado.itens_orcamento
                                                 select i1.OrcamentoCod).Contains(i.OrcamentoCod)
                                         select i).ToList();
                    var itensAdicionados = (from i in orcAlterado.itens_orcamento
                                            where !(from i1 in orcAtual.itens_orcamento
                                                    select i1.OrcamentoCod).Contains(i.OrcamentoCod)
                                            select i).ToList();
                    itensDeletados.ForEach(i => orcAtual.itens_orcamento.Remove(i));
                    foreach (itens_orcamento item in itensAdicionados)
                    {
                        if (contexto.Entry(item).State == System.Data.Entity.EntityState.Detached)
                            contexto.itens_orcamento.Attach(item);
                        orcAtual.itens_orcamento.Add(item);
                    }

                    contexto.Entry(orcAtual).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao editar um orçamento: " + ex.Message);
#endif
                return -1;
            }
        }

        internal int AprovarOrcamento(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    orcamento o = contexto.orcamento.Find(cod);
                    o.Status = 2;
                    contexto.Entry(o).State = System.Data.Entity.EntityState.Modified;
                    contexto.SaveChanges();
                    decimal valorTotal = o.ValorTotal;                    
                    if(o.Parcelas > 1)
                    {
                        //Quer dizer que a compra é parcelada
                        //Insiro as contas a receber com datas e valores parcelados
                        decimal valorParcela = (valorTotal / o.Parcelas);
                        DateTime dataVencto = DateTime.Now;
                        for (int i = 0; i < o.Parcelas; i++)
                        {
                            //considerando que a primeira parcela é sempre 30 dias após a aprovação do orçamento
                            dataVencto.AddMonths(1);
                            conta_receber conta = new conta_receber
                            {
                                DtVencto = dataVencto,
                                FormaPagamento = "Cartão de Crédito",
                                OrcamentoCod = o.Cod,
                                PacienteCod = o.PacienteCod,
                                Tipo = "Orçamento",
                                Valor = valorParcela
                            };
                            contexto.conta_receber.Add(conta);
                        }
                    }
                    else
                    {
                        conta_receber con = new conta_receber
                        {
                            DtVencto = DateTime.Now,
                            FormaPagamento = "Dinheiro/À vista",
                            OrcamentoCod = o.Cod,
                            PacienteCod = o.PacienteCod,
                            Tipo = "Orçamento",
                            Valor = o.ValorTotal
                        };
                        contexto.conta_receber.Add(con);
                    }
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        internal List<orcamento> ObterPorPaciente(int pacienteCod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.orcamento.Include("dentista").Include("paciente")
                        .Include("funcionario").Where(o => o.PacienteCod == pacienteCod).ToList();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        internal List<orcamento> Obter()
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.orcamento.Include("dentista").Include("paciente").Include("funcionario").ToList();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao obter os orçamentos. " + ex.Message);
#endif
                return null;
            }
        }

        internal orcamento ObterPorCod(int codOrcamento)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    return contexto.orcamento.Include("dentista").Include("paciente").Include("funcionario")
                        .Where(o => o.Cod == codOrcamento).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    orcamento o = contexto.orcamento.Find(cod);
                    if(o.Status == 0)
                    {
                        contexto.orcamento.Attach(o);

                        //Criando uma lista auxiliar de itens_orcamento que estão atralados ao objeto orcamento
                        //a ser excluído. Isso é necesário para não dar erro de integridade referencial na tabela
                        //muitos para muitos
                        List<itens_orcamento> auxItens = null;
                        if (o.itens_orcamento != null)
                        {
                            auxItens = o.itens_orcamento.ToList();
                            //Removendo os objetos itens_orcamento da lista
                            //atrelada ao objeto orcamento. A lista de itens_orcamento
                            //ficará vazia
                            auxItens.ForEach(i => o.itens_orcamento.Remove(i));
                        }
                        //Quando o EF percebe que ele tem uma lista vazia atrelada a um muitos para muitos ele deleta
                        //as referências da tabela MM relacionada antes de deletar o orcamento
                        contexto.orcamento.Remove(o);
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //MSG USUÁRIO
                        return -5;
                    }
                    
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao excluir um orcamento. " + ex.Message);
#endif
                return -1;
            }
        }
    }
}