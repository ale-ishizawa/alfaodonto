﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    internal class UsuarioDAO
    {
        internal usuario Autenticar(string login, string senha)
        {
            using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
            {
                try
                {
                    return contexto.usuario.Where(x => x.Login == login && x.Senha == senha).FirstOrDefault();
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw;
#endif
                    return null;
                }
            }
        }
    }
}