﻿using AldaOdonto.Model;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.DAL
{
    internal class DentistaDAO
    {
        internal int Gravar(dentista den)
        {
            try
            {   
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    contexto.dentista.Add(den);
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {                                                                       
#if DEBUG
                throw new Exception("Erro ao tentar gravar um novo dentista. " + ex.Message);
#endif
                return -1;
            }
        }

        internal string[] Pesquisar(string nome)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    List<dentista> _dentistas;
                    _dentistas = contexto.dentista.Where(d => d.Nome.Contains(nome)).ToList();//&& x.usuario.Ativo == 1)
                    List<string> dentistas = new List<string>();
                    foreach (dentista d in _dentistas)
                    {
                        dentistas.Add(string.Format("{0}-{1}", d.Nome, d.CodDentista));
                    }
                    return dentistas.ToArray();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar pesquisar um dentista. " + ex.InnerException);
#endif
                return null;
            }
        }

        internal int Excluir(int cod)
        {
            try
            {
                using(clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    dentista d = contexto.dentista.Where(x => x.CodDentista == cod).FirstOrDefault();
                    if (d != null)
                    {
                        //usuario fica inativo
                        d.Ativo = 2;
                        contexto.Entry(d).State = System.Data.Entity.EntityState.Modified;
                        return contexto.SaveChanges();
                    }
                    else
                    {
                        //Mensagem para o usuário, -5
                        return -5;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar inativar um dentista. " + ex.InnerException);
#endif
                return -1;
            }
        }

        internal int Editar(dentista den)
        {
            try
            {
                using (clinicaEntities contexto = new clinicaEntities(Util.StringConexao))
                {
                    dentista d = contexto.dentista.Find(den.CodDentista);
                    d.CodDentista = den.CodDentista;
                    d.ClinicaCod = den.ClinicaCod;
                    d.Bairro = den.Bairro;
                    d.Cep = den.Cep;
                    d.Cidade = den.Cidade;
                    d.Comissao = den.Comissao;
                    d.Complemento = den.Complemento;
                    d.Cpf = den.Cpf;
                    d.Cro = den.Cro;
                    d.DtNascto = den.DtNascto;
                    d.Estado = den.Estado;
                    d.Logradouro = den.Logradouro;
                    d.Nome = den.Nome;
                    d.Numero = den.Numero;
                    d.Rg = den.Rg;
                    d.SiglaConselho = den.SiglaConselho;
                    d.Celular = den.Celular;
                    d.TelResidencia = den.TelResidencia;
                    d.TempoConsulta = den.TempoConsulta;
                    d.UfConselho = den.UfConselho;
                    d.Email = den.Email;       
                    contexto.Entry(d).State = System.Data.Entity.EntityState.Modified;
                    return contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new Exception("Erro ao tentar editar um dentista. " + ex.InnerException);
#endif
                return -1;
            }
        }
    }
}