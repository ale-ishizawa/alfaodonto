﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class OrcamentoViewModel
    {
        public int Cod { get; set; }
        public DateTime DtInicio { get; set; }
        public DateTime? DtFim { get; set; }
        public int Status { get; set; }
        public string Tipo { get; set; }
        public int? FuncionarioCod { get; set; }
        public int DentistaCod { get; set; }
        public int PacienteCod { get; set; }
        public List<ItensOrcamentoViewModel> ItensOrcamento { get; set; }
        public string NomeDentista { get; set; }
        public string NomeFuncionario { get; set; }
        public string NomePaciente { get; set; }
        public string Procedimento { get; set; }
        public int Parcelas { get; set; }
        public decimal? ValorEntrada { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal? Desconto { get; set; }
    }
}