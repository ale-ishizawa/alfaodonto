﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class AtendimentoViewModel
    {
        public int AgendamentoCod { get; set; }
        public int? OrcamentoCod { get; set; }
        public int? ProcedimentoCod { get; set; }
        public string Descricao { get; set; }
        public DateTime? Data { get; set; }
    }
}