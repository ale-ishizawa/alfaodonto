﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class ProcedimentoViewModel
    {
        public int Cod { get; set; }
        public string Descricao { get; set; }
        public int EspecialidadeCod { get; set; }
        public decimal? Valor { get; set; }
        public string EspecialidadeDescricao { get; set; }

       
    }
}