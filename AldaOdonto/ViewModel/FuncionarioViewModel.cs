﻿using AldaOdonto.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class FuncionarioViewModel
    {
        public int Cod { get; set; }
        public string Email { get; set; }
        public string Grupo { get; set; }
        public string TelResidencia { get; set; }
        public string TelCelular { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Nome { get; set; }
        public int ClinicaCod { get; set; }   
        public string Login { get; set; }
        public string Senha { get; set; }
        public int Ativo { get; set; }     
        public DateTime DtNascto { get; set; }

    }
}