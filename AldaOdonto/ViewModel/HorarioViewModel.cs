﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class HorarioViewModel
    {
        public int Cod { get; set;}
        public DateTime Data { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int HoraIntervalo { get; set; }
        public int UsuarioCod { get; set; }
    }
}