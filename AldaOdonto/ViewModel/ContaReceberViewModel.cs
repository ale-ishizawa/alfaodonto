﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class ContaReceberViewModel
    {
        public int Cod { get; set; }
        public DateTime DtVencto { get; set; }
        public string Tipo { get; set; }
        public string FormaPagamento { get; set; }
        public decimal? Valor { get; set; }
        public decimal? SaldoDevedor { get; set; }
        public DateTime? DtPagto { get; set; }
        public int? OrcamentoCod { get; set; }
        public int? PacienteCod { get; set; } 
        public string PacienteNome { get; set; }

    }
}