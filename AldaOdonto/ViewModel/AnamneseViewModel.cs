﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class AnamneseViewModel
    {
        public string Nome { get; set; }
        public int Cod { get; set; }
    }
}