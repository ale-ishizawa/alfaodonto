﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class MovimentoEstoqueViewModel
    {
        public int Cod {get; set;}
        public DateTime Data { get; set; }
        public int ProdutoCod { get; set; }
        public int DentistaCod { get; set; }
        public int FuncionarioCod { get; set; }
        public string Observacao { get; set; }
        public int Quantidade { get; set; }
        public int Tipo { get; set; }

    }
}