﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class PerguntaViewModel
    {
        public int Cod { get; set; }
        public string Descricao { get; set; }
        public int Ativa { get; set; }
        public int Tipo { get; set; }
        public int Alerta { get; set; }
        public string AlertaDescricao { get; set; }
        public string AlertaQuando { get; set; }
        
    }
}