﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class EspecialidadeViewModel
    {
        public int Cod { get; set; }
        public string Descricao { get; set; }
    }
}