﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class AgendamentoViewModel
    {
        public int Cod { get; set; }
        public string Tipo { get; set; }
        public int Tempo { get; set; }
        public DateTime Data { get; set; }
        public string Observacao { get; set; }
        public int DentistaCod { get; set; }
        public int FuncionarioCod { get; set; }
        public int PacienteCod { get; set; }
    }
}