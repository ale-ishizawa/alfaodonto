﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class ItensOrcamentoViewModel
    {
        public int OrcamentoCod { get; set; }
        public int ProcedimentoCod { get; set; }
        public int? ItemQtd { get; set; }
        public decimal ItemValor { get; set; }
        public string DenteRegiao { get; set; }
        public string ProcedimentoNome { get; set; }

    }
}