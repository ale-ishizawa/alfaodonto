﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class ProdutoViewModel
    {

        public int Cod { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }

        //PADRÃO DA QTD CRÍTICA É 0
        public int QtdCritica { get; set; }
    }
}