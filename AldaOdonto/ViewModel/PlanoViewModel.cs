﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class PlanoViewModel
    {
        public int Cod { get; set; }
        public decimal Valor { get; set; }
        public string Descricao { get; set; }
        public string Padrao { get; set; }
        public List<PlanoProcedimentosViewModel> PlanoProcedimentos { get; set; }
    }
}