﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.ViewModel
{
    public class PacienteRespostasViewModel
    {
        public int PerguntaCod { get; set; }
        public int AnamneseCod { get; set; }
        public int UsuarioCod { get; set; }
        public string Alerta { get; set; }
        public string Resposta { get; set; }
    }
}