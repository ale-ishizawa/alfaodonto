﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class paciente
    {

        public string[] PesquisarPaciente(string nome)
        {
            return new PacienteDAO().PesquisarPaciente(nome);
        }
        public int Gravar(paciente p)
        {
            //Validações
            if (!string.IsNullOrWhiteSpace(p.ClinicaCod.ToString()) && !string.IsNullOrWhiteSpace(p.Nome) &&
                !string.IsNullOrWhiteSpace(p.DtNascto.ToString()) && p.EstadoCivil != "--Estado Civil--" && !string.IsNullOrWhiteSpace(p.Logradouro)
                && !string.IsNullOrWhiteSpace(p.Genero) && !string.IsNullOrWhiteSpace(p.Cep) && !string.IsNullOrWhiteSpace(p.Numero)
                && !string.IsNullOrWhiteSpace(p.Complemento) && !string.IsNullOrWhiteSpace(p.Bairro) && !string.IsNullOrWhiteSpace(p
                .Cidade) && !string.IsNullOrWhiteSpace(p.Estado) && !string.IsNullOrWhiteSpace(p.Celular) && !string.IsNullOrWhiteSpace(p
                .TelRecado) && !string.IsNullOrWhiteSpace(p.NomeRecado) && !string.IsNullOrWhiteSpace(p.Email) && !string.IsNullOrWhiteSpace(p
                .Cpf) && !string.IsNullOrWhiteSpace(p.Rg))
            {
                //VALIDADO!
                return new PacienteDAO().Gravar(p);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Editar()
        {
            //Validações
            if(this.CodPaciente <= 0)
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
            else
            {
                return new PacienteDAO().Editar(this);
            }
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new PacienteDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }
    }
}