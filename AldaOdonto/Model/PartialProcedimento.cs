﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class procedimento
    {
        public int Gravar()
        {
            if(this.Descricao.Length >= 3)
            {
                return new ProcedimentoDAO().Gravar(this);
            }
            else
            {
                return -10;
            }
        }

        public int Editar()
        {
            if(this.Cod > 0 && !string.IsNullOrWhiteSpace(this.Valor.ToString()) && this.EspecialidadeCod > 0)
            {
                return new ProcedimentoDAO().Editar(this);
            }
            else
            {
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            return new ProcedimentoDAO().Excluir(cod);
        }

        public string[] Pesquisar(string nome)
        {
            return new ProcedimentoDAO().Pesquisar(nome);
        }

        public List<procedimento> ObterProcedimentos()
        {
            return new ProcedimentoDAO().ObterProcedimentos();
        }

        public procedimento ObterPorCod(int cod)
        {
            if(cod > 0)
            {
                return new ProcedimentoDAO().ObterPorCod(cod);
            }
            else
            {
                return null;
            }
        }
    }
}