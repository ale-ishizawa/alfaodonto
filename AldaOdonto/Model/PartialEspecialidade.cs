﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class especialidade
    {

        public int Gravar()
        {
            if (this.Descricao.Length >= 3)
                return new EspecialidadeDAO().Gravar(this);
            else
                return -10;
        }

        public int Excluir(int cod)
        {
            if (cod > 0)
                return new EspecialidadeDAO().Excluir(cod);
            else
                return -10;
        }

        public List<especialidade> ObterEspecialidades()
        {
            return new EspecialidadeDAO().ObterEspecialidades();
        }
    }
}