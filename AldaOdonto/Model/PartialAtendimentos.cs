﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class atendimentos
    {

        public int FinalizarAtendimento()
        {
            if(this.AgendamentoCod > 0)
            {
                return new AtendimentosDAO().FinalizarAtendimento(this);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }
    }
}