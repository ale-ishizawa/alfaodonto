﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class clinica
    {
        public int Gravar()
        {
            if(this.Cod == 0)
            {
                return new ClinicaDAO().Gravar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Editar()
        {
            if(this.Cod > 0)
            {
                return new ClinicaDAO().Editar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new ClinicaDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public clinica VerificarCadastro()
        {
            return new ClinicaDAO().VerificarCadastro();
        }
    }
}