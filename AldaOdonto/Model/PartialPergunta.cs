﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class pergunta
    {
        public int Gravar()
        {
            if (this.Cod == 0 && !string.IsNullOrWhiteSpace(this.Descricao) && !string.IsNullOrWhiteSpace(this.Tipo.ToString()) &&
                !string.IsNullOrWhiteSpace(this.Alerta.ToString()))
            {
                //VALIDADO!
                return new PerguntaDAO().Gravar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Editar()
        {
            if (this.Cod > 0)
            {
                return new PerguntaDAO().Editar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new PerguntaDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public List<pergunta> ObterPerguntas()
        {
            return new PerguntaDAO().ObterPerguntas();
        }

        public pergunta ObterPorCod(int cod)
        {
            return new PerguntaDAO().ObterPorCod(cod);
        }
    }
}