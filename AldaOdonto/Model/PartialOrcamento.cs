﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class orcamento
    {

        public int Gravar()
        {
            if(!string.IsNullOrWhiteSpace(this.DtInicio.ToString()) && !string.IsNullOrWhiteSpace(this.Status.ToString()) && !string.IsNullOrWhiteSpace(this.Tipo)
                    && !string.IsNullOrWhiteSpace(this.DentistaCod.ToString()) && !string.IsNullOrWhiteSpace(this.PacienteCod.ToString()) &&
                    this.itens_orcamento != null)
            {
                return new OrcamentoDAO().Gravar(this);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
                
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new OrcamentoDAO().Excluir(cod);
            }
            else
            {
                return -10;
            }
        }

        public int Editar()
        {
            if (this.Cod > 0 && !string.IsNullOrWhiteSpace(this.DtInicio.ToString()) && !string.IsNullOrWhiteSpace(this.Status.ToString()) && !string.IsNullOrWhiteSpace(this.Tipo)
                    && !string.IsNullOrWhiteSpace(this.DentistaCod.ToString()) && !string.IsNullOrWhiteSpace(this.PacienteCod.ToString()) &&
                    this.itens_orcamento != null)
            {
                return new OrcamentoDAO().Editar(this);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }

        public List<orcamento> Obter()
        {
            return new OrcamentoDAO().Obter();
        }

        public List<orcamento> ObterPorPaciente(int pacienteCod)
        {
            return new OrcamentoDAO().ObterPorPaciente(pacienteCod);
        }

        public orcamento ObterPorCod(int codOrcamento)
        {
            if(codOrcamento > 0)
            {
                return new OrcamentoDAO().ObterPorCod(codOrcamento);
            }
            else
            {
                return null;
            }
        }

        public int AprovarOrcamento(int cod)
        {
            if(cod > 0)
            {
                return new OrcamentoDAO().AprovarOrcamento(cod);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }
    }
}