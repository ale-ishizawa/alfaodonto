﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class conta_receber
    {

        public int BaixarContaReceber(int cod)
        {
            if (cod > 0)
            {
                return new ContaReceberDAO().BaixarContaReceber(cod);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }

        public List<conta_receber> ObterContas()
        {
            return new ContaReceberDAO().ObterContas();
        }
    }
}