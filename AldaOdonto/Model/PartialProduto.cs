﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class produto
    {
        public int Gravar()
        {
            if (this.Descricao.Length > 3 && this.Quantidade >= 0)
                return new ProdutoDAO().Gravar(this);
            else            
                return -10;                     
        }
        
        public int Editar()
        {
            if(this.Cod > 0 && !string.IsNullOrWhiteSpace(this.Descricao) && !string.IsNullOrWhiteSpace(this.Quantidade.ToString()) &&
                !string.IsNullOrWhiteSpace(this.QtdCritica.ToString()))
            {
                return new ProdutoDAO().Editar(this);
            }
            else
            {
                //Mensagem pro Usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if (cod > 0)
                return new ProdutoDAO().Excluir(cod);
            else
                return -10;
        }

        public int BaixarEstoque()
        {
            if(this.Quantidade >= 0 && this.QtdCritica >= 0)
            {
                return new ProdutoDAO().BaixarEstoque(this);
            }
            else
            {
                return -10;
            }
        }

        public List<produto> ObterProdutos()
        {
            return new ProdutoDAO().ObterProdutos();
        }

        public produto ObterPorCod(int cod)
        {
            return new ProdutoDAO().ObterPorCod(cod);
        }
    }
}