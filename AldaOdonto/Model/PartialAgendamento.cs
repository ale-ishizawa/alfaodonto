﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class agendamento
    {
        public int Gravar(List<itens_orcamento> _itensOrcamento)
        {
            if(!string.IsNullOrWhiteSpace(this.TempoConsulta.ToString()) && !string.IsNullOrWhiteSpace(this.Tipo) && !string.IsNullOrWhiteSpace(this.Data.ToString())
                && this.paciente.CodPaciente > 0)
            {
                //Validado
                return new AgendamentoDAO().Gravar(this, _itensOrcamento);
            }
            else
            {
                //Mensagem para o usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new AgendamentoDAO().Excluir(cod);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }

        public int Editar()
        {
            if (!string.IsNullOrWhiteSpace(this.TempoConsulta.ToString()) && !string.IsNullOrWhiteSpace(this.Tipo) && !string.IsNullOrWhiteSpace(this.Data.ToString())
                && this.paciente.CodPaciente > 0 && this.dentista.CodDentista > 0)
            {
                return new AgendamentoDAO().Editar(this);
            }
            else
            {
                //Mensagem usuário
                return -10;
            }
        }

        public List<agendamento> ObterPorCod(int cod)
        {
            if(cod > 0)
            {
                return new AgendamentoDAO().ObterPorCod(cod);
            }
            else
            {
                //MSG USUÁRIO
                return null;
            }
        }

        public List<agendamento> Obter()
        {
            return new AgendamentoDAO().Obter();
        }
    }
}