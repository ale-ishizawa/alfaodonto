﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class usuario
    {
        public usuario Autenticar(string login, string senha)
        {
            if (login.Length > 3 && !string.IsNullOrWhiteSpace(senha))
                return new UsuarioDAO().Autenticar(login, senha);
            else
                return null;
        }

    }
}