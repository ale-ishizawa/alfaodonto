﻿using AldaOdonto.DAL;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class anamnese
    {

        public int Gravar()
        {
            if(this.Cod == 0 && this.Nome.Length > 3)
            {
                return new AnamneseDAO().Gravar(this);
            }
            else
            {
                //Erro, mensagem pro usuário
                return -10;
            }
        }

        public List<anamnese> ObterAnamneses()
        {
            return new AnamneseDAO().ObterAnamneses();
        }

        public anamnese ObterPorCod(int cod)
        {
            return new AnamneseDAO().ObterPorCod(cod);
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new AnamneseDAO().Excluir(cod);
            }
            else
            {
                //msg para o usuário
                return -10;
            }
        }

        public int Editar()
        {
            if(this.Nome.Trim().Length > 2 && this.Cod > 0)
            {
                return new AnamneseDAO().Editar(this);
            }
            else
            {
                //msg para o usuário
                return -10;
            }
        }
    }
}