﻿using AldaOdonto.DAL;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class funcionario
    {
        public int Gravar(funcionario f)
        {
           // string msg = "";
           //VALIDANDO SE ALGUM DESSES CAMPOS ESTÃO BRANCO OU NULL
            if(!string.IsNullOrWhiteSpace(f.Bairro) && !string.IsNullOrWhiteSpace(f.Cep) && !string.IsNullOrWhiteSpace(f.Cidade)
                && !string.IsNullOrWhiteSpace(f.Complemento) && !string.IsNullOrWhiteSpace(f.Email) && !string.IsNullOrWhiteSpace(
                    f.Estado) && !string.IsNullOrWhiteSpace(f.Logradouro) && !string.IsNullOrWhiteSpace(f.Nome) &&
                    !string.IsNullOrWhiteSpace(f.Numero) && !string.IsNullOrWhiteSpace(f.Celular) && !string.IsNullOrWhiteSpace(
                        f.Login) && !string.IsNullOrWhiteSpace(f.Senha))
            {
                return new FuncionarioDAO().Gravar(f);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Editar()
        {
            if(this.CodFuncionario > 0)
            {
                return new FuncionarioDAO().Editar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if(this.CodFuncionario > 0)
            {
                return new FuncionarioDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public List<FuncionarioViewModel> Pesquisar(string nome)
        {
            if (nome.Trim().Length >= 1)
            {
                return new FuncionarioDAO().Pesquisar(nome);
            }
            else
            {
                return null;
            }
        }
    }
}