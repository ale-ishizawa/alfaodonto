﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class movimento_estoque
    {
        public int MovimentoEstoque()
        {
            if(this.ProdutoCod > 0 && this.Quantidade > 0)
            {
                return new MovimentoEstoqueDAO().MovimentoEstoque(this);
            }
            else
            {
                //MSG USUÁRIO
                return -10;
            }
        }
    }
}