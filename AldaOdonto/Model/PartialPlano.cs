﻿using AldaOdonto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class plano
    {
        public int Gravar()
        {
            if(this.Cod == 0)
            {
                return new PlanoDAO().Gravar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Editar()
        {
            if(this.Cod > 0)
            {
                return new PlanoDAO().Editar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if (cod > 0)
            {
                return new PlanoDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }
    }
}