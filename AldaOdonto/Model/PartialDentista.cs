﻿using AldaOdonto.DAL;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AldaOdonto.Model
{
    public partial class dentista
    {
        public int Gravar(dentista d)
        {
            if(!string.IsNullOrWhiteSpace(d.Cro) && !string.IsNullOrWhiteSpace(d.ClinicaCod.ToString())
                && !string.IsNullOrWhiteSpace(d.SiglaConselho) && !string.IsNullOrWhiteSpace(d.Email) &&
                !string.IsNullOrWhiteSpace(d.Celular) && !string.IsNullOrWhiteSpace(d.Cep) && !string.IsNullOrWhiteSpace
                (d.Logradouro) && !string.IsNullOrWhiteSpace(d.Numero) && !string.IsNullOrWhiteSpace(d.Complemento)
                && !string.IsNullOrWhiteSpace(d.Bairro) && !string.IsNullOrWhiteSpace(d.Cidade) && !string.IsNullOrWhiteSpace
                (d.Estado) && !string.IsNullOrWhiteSpace(d.UfConselho) && !string.IsNullOrWhiteSpace(d.Nome) &&
                !string.IsNullOrWhiteSpace(d.TempoConsulta.ToString()) && !string.IsNullOrWhiteSpace(d.Cpf) && 
                !string.IsNullOrWhiteSpace(d.DtNascto.ToString()))
            {
                //VALIDADO!
                return new DentistaDAO().Gravar(d);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }


        public string[] Pesquisar(string nome)
        {
            if (nome.Trim().Length >= 1)
            {
                return new DentistaDAO().Pesquisar(nome);
            }
            else
            {
                return null;
            }
        }

        public int Editar()
        {
            if (this.CodDentista > 0)
            {
                return new DentistaDAO().Editar(this);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

        public int Excluir(int cod)
        {
            if(cod > 0)
            {
                return new DentistaDAO().Excluir(cod);
            }
            else
            {
                //Erro, retornar mensagem para usuário
                return -10;
            }
        }

    }
}