﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Produtos.aspx.cs" Inherits="AlfaOdontoWeb.Produtos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
     <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">
        function openModal() {
            $('#defaultModal').modal('show');
        };
        function openModalSmall() {
            $('#smallModal').modal('show');
        };
        function closeModal() {
            $('#defaultModal').modal('close');
        };
        function closeModalSmall() {
            $('#smallModal').modal('close');
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PRODUTOS</h2>
            </div>
           
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Novo Produto
                                <small></small>
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                       <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">CADASTRAR PRODUTO</button> 
                                     </div>                                   
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div><!-- Fim da Linha -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Produtos Cadastrados
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <asp:GridView runat="server" CssClass="table table-striped" AutoGenerateColumns="False" ID="gdvProdutos" GridLines="None" PageSize="10"
                                 OnPageIndexChanging="gdvProdutos_PageIndexChanging" AllowPaging="true" >
                                <Columns>
                                    <asp:BoundField DataField="Cod" HeaderText="#" />
                                    <asp:BoundField DataField="Descricao" HeaderText="Descrição" />
                                    <asp:BoundField DataField="Quantidade" HeaderText="Quantidade em Estoque" />
                                    <asp:BoundField DataField="QtdCritica" HeaderText="*Quantidade Crítica" />                                   
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnExcluir" CssClass="btn bg-red waves-effect"  OnClientClick="return confirm(&quot;Confirma a exclusão do produto?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnExcluir_Command" formnovalidate="formnovalidate" >
                                            </asp:Button>
                                            <asp:Button ID="btnEditar" runat="server" CausesValidation="false" CssClass="btn bg-blue waves-effect" Text="Editar" CommandArgument='<%# Eval("Cod") %>'
                                                  formnovalidate="formnovalidate" OnCommand="btnEditar_Command">
                                            </asp:Button>
                                            <asp:Button Text="BAIXAR" CssClass="btn bg-green waves-effect" ID="btnBaixarEstoque" OnCommand="btnBaixarEstoque_Command" runat="server"
                                                formnovalidate="formnovalidate" CommandArgument='<%# Eval("Cod") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
         </div>
        </section>    
         <!-- Default Size -->
         <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" >
             <input type="hidden" id="codProduto" name="codProduto" runat="server" />      
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Novo Produto</h4>
                        </div>
                        <div class="modal-body">
                           <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="txtDescricao">Descrição*</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtDescricao" required="required"/>                                                
                                            </div>
                                     </div>
                                </div>
                             </div>
                              <div class="row clearfix">
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                   <label for="txtQtd">Quantidade*</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" runat="server" id="txtQtd" required="required"/>                                                 
                                            </div>
                                     </div>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                   <label for="txtQtdCritica">Quantidade Crítica</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" runat="server" id="txtQtdCritica" required="required" />                                                
                                            </div>
                                     </div>
                                </div>
                            </div> <!-- Fim da Linha-->
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" ID="btnCadastrar" CssClass="btn bg-green waves-effect" runat="server" OnClick="btnCadastrar_Click" />
                            <asp:Button Text="SALVAR" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" Visible="false" Enabled="false" />                            
                            <asp:Button Text="FECHAR" CssClass="btn btn-link waves-effect" runat="server" ID="btnFechar" OnClick="btnFechar_Click" formnovalidate="formnovalidate" />
                        </div>
                    </div>
                </div>
            </div><!-- Fim do Modal Default -->
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="smallModalLabel">Baixar Estoque</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="txtMovimento">Entre com a quantidade a ser baixada*</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" runat="server" id="txtMovimento"/>                                                
                                            </div>
                                     </div>
                                </div>
                             </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="BAIXAR" ID="btnBaixar" runat="server" OnClick="btnBaixar_Click" CssClass="btn bg-green waves-effect" formnovalidate="formnovalidate" />
                            <asp:Button Text="FECHAR" runat="server" ID="btnFecharBaixaEstoque" CssClass="btn btn-link" OnClick="btnFecharBaixaEstoque_Click" formnovalidate="formnovalidate"/>
                        </div>
                    </div>
                </div>
            </div>
            
         <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
      <!-- Input Mask Plugin Js -->
    <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
     <%--<script src="js/pages/forms/advanced-form-elements.js"></script>--%>
    
    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

</asp:Content>
