﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Funcionarios.aspx.cs" Inherits="AlfaOdontoWeb.Funcionarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Colorpicker Css -->
    <link href="../../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="../../plugins/dropzone/dropzone.css" rel="stylesheet"/>

    <!-- Multi Select Css -->
    <link href="../../plugins/multi-select/css/multi-select.css" rel="stylesheet"/>

    <!-- Bootstrap Spinner Css -->
    <link href="../../plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet"/>

    <!-- Bootstrap Tagsinput Css -->
    <link href="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"/>

    <!-- noUISlider Css -->
    <link href="../../plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <script type="text/javascript">        

        function closeModal() {
            $('#defaultModal').modal('close');
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>FUNCIONÁRIOS</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Pesquisar Funcionário ou Cadastrar Novo
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body blue">                            
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNome"/>
                                                <label class="form-label">Buscar Funcionário</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                                        <br />OU                                  
                                    </div>                                   
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br />
                                        <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">CADASTRAR NOVO FUNCIONÁRIO</button>    
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           A especialidade foi cadastrada com sucesso.
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    <!-- Large Size MODAL -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <input type="hidden" id="codFuncionario" name="codFuncionario" runat="server" />
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Dados do Funcionário</h4>
                        </div>
                        <div class="modal-body">
                            <div class="demo-masked-input">
                            <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="txtFuncionario">Nome*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtFuncionario" required="required"/>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="txtEmail">Email*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" class="form-control email" runat="server" id="txtEmail" required="required"/>
                                          </div>
                                        </div>
                                    </div>                              
                            </div> <!-- End Row -->
                            <div class="row clearfix">     
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtDtNascto">Data de Nascimento*</label>
                                       <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" class="form-control date" runat="server" id="txtDtNascto" required="required"/>
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtCelular">Celular*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control mobile-phone-number" runat="server" id="txtCelular" required="required"/>
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtTelefoneResidencia">Telefone Residencial</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control phone-number" runat="server" id="txtTelefoneResidencia"/>
                                            </div>
                                        </div>
                                 </div>                                
                            </div> <!-- End Row -->                         
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">DADOS DE ACESSO</a></li>
                                        <li role="presentation"><a href="#settings_animation_1" data-toggle="tab">ENDEREÇO</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                          <div class="row clearfix">                                            
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                 <label for="txtLogin">Login*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtLogin" required="required"/>                                 
                                                </div>
                                              </div>
                                             </div>
                                            <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <label for="txtSenha">Senha*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" runat="server" id="txtSenha" required="required"/>                                                    
                                                </div>
                                              </div>
                                             </div>
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <div class="form-group">
                                                 <br />
                                                    <div class="switch panel-switch-btn">
                                                        <span class="m-r-10 font-12">Ativo?</span>
                                                        <label>Não<input type="checkbox" id="ckbAtivo" checked="checked" runat="server" /><span class="lever switch-col-cyan"></span>Sim</label>
                                                    </div>                                                    
                                              </div>
                                             </div>
                                           </div>  <!-- End Row -->                                               
                                        </div>                                        
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                          <div class="row clearfix">
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                 <label for="txtCep">CEP*</label>
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                        <input type="text" maxlength="9" class="form-control" runat="server" id="txtCep" required="required"/>                                                        
                                                      </div>
                                                   </div>
                                               </div>                                                 
                                             <div class="col-lg-8 col-md-8 col-sm8 col-xs-8">
                                                 <label for="txtLogradouro">Logradouro</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtLogradouro" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div>      
                                          </div> <!-- End Row-->  
                                          <div class="row clearfix">
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <label for="txtNumero">Número</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtNumero" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div> 
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <label for="txtComplemento">Complemento</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtComplemento" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                   <label for="txtBairro">Bairro</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtBairro" required="required"/>                                                       
                                                     </div>
                                                 </div>                                                 
                                             </div>   
                                          </div> <!-- End Row -->
                                          <div class="row clearfix">
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <label for="txtCidade">Cidade</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtCidade" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6"> 
                                                  <label for="ddlEstados">Cidade</label>
                                                  <div class="form-group">                                                  
                                                  <asp:DropDownList runat="server" ID="ddlEstados" CssClass="form-control show-tick">
                                                      <asp:ListItem Text="--Selecione--" Value="0" />    
                                                      <asp:ListItem Text="Acre" Value="AC" /> 
                                                      <asp:ListItem Text="Alagoas" Value="AL" /> 
                                                      <asp:ListItem Text="Amazonas" Value="AM" /> 
                                                      <asp:ListItem Text="Amapá" Value="AP" /> 
                                                      <asp:ListItem Text="Bahia" Value="BA" /> 
                                                      <asp:ListItem Text="Ceará" Value="CE" /> 
                                                      <asp:ListItem Text="Distrito Federal" Value="DF" /> 
                                                      <asp:ListItem Text="Espírito Santo" Value="ES" /> 
                                                      <asp:ListItem Text="Goiás" Value="GO" /> 
                                                      <asp:ListItem Text="Maranhão" Value="MA" /> 
                                                      <asp:ListItem Text="Mato Grosso" Value="MT" /> 
                                                      <asp:ListItem Text="Mato Grosso do Sul" Value="MS" /> 
                                                      <asp:ListItem Text="Minas Gerais" Value="MG" /> 
                                                      <asp:ListItem Text="Pará" Value="PA" /> 
                                                      <asp:ListItem Text="Paraíba" Value="PB" /> 
                                                      <asp:ListItem Text="Paraná" Value="PR" /> 
                                                      <asp:ListItem Text="Pernambuco" Value="PE" /> 
                                                      <asp:ListItem Text="Piauí" Value="PI" /> 
                                                      <asp:ListItem Text="Rio de Janeiro" Value="RJ" /> 
                                                      <asp:ListItem Text="Rio Grande do Norte" Value="RN" /> 
                                                      <asp:ListItem Text="Rondônia" Value="RO" /> 
                                                      <asp:ListItem Text="Rio Grande do Sul" Value="RS" /> 
                                                      <asp:ListItem Text="Roraíma" Value="RR" />    
                                                      <asp:ListItem Text="Santa Catarina" Value="SC" /> 
                                                      <asp:ListItem Text="Sergipe" Value="SE" /> 
                                                      <asp:ListItem Text="São Paulo" Value="SP" />    
                                                      <asp:ListItem Text="Tocantins" Value="TO" />                                              
                                                  </asp:DropDownList>                              
                                                 </div>                                              
                                             </div>  
                                          </div>
                                       </div>                                     
                                    </div>
                                </div>                                
                            </div> <!-- #END# Tabs With Custom Animations -->
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="div1" visible="false">
                                        <div class="" runat="server" id="div2">
                                           Preencha os campos obrigatórios *.
                                        </div>
                                    </div>                                               
                            </div> 
                        </div>
                      </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" ID="btnCadastrar" OnClick="btnCadastrar_Click" CssClass="btn bg-green waves-effect" runat="server" />
                            <asp:Button Text="SALVAR" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" Visible="false" Enabled="false" />                            
                            <asp:Button Text="FECHAR" CssClass="btn btn-link waves-effect" runat="server" ID="btnFechar" data-dismiss="modal" OnClick="btnFechar_Click"/>                           
                        </div>
                    </div>
                </div>
                </div>           
      <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Colorpicker Js -->
    <script src="../../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="../../plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/forms/advanced-form-elements.js"></script>

</asp:Content>
