﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Agendamentos.aspx.cs" Inherits="AlfaOdontoWeb.Agendamentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <link href='plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
  <link href='plugins/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
  <script src="plugins/jquery/jquery.min.js"></script>
  <script src='plugins/momentjs/moment.js'></script>
  <script src='plugins/fullcalendar/fullcalendar.js'></script>
  <script src='plugins/fullcalendar/locale-all.js'></script>
 
  <script>
  $(document).ready(function() {
    // page is now ready, initialize the calendar...
    var initialLocaleCode = 'pt-br';

    $('#calendar').fullCalendar({
        // put your options and callbacks here
        customButtons: {
            myCustomButton: {
                text: 'custom!',
                click: function () {
                    alert('clicked the custom button!');
                }
            }
        },
			header: {
			    left: 'prev,next today myCustomButton',
			    center: 'title',
			    right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2017-05-12',
			locale: initialLocaleCode,
			buttonIcons: true, // show the prev/next text
			weekNumbers: true,            
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			customButtons: true          
			
		});
  });

  </script>
  <style>

  	body {
  		margin: 40px 10px;
  		padding: 0;
  		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
  		font-size: 14px;
        background-color: #ffffff;
  	}

  	#calendar {
  		max-width: 1200px;
  		margin: 0 auto;
        
  	}

  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div id="calendar">

  </div>
</asp:Content>
