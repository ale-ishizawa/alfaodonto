﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Orcamentos : System.Web.UI.Page
    {
        List<ItensOrcamentoViewModel> _itensOrc = new List<ItensOrcamentoViewModel>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CarregarGridOrcamentos(0);
            if (!IsPostBack)
            {
                _itensOrc = null;
            }
        }
        [WebMethod]
        public static string[] PesquisarDentista(string nome)
        {
            string[] dentistas = new DentistaController().Pesquisar(nome);
            return dentistas.ToArray();

        }
        [WebMethod]
        public static string[] PesquisarPaciente(string nome)
        {
            string[] pacientes = new PacienteController().PesquisarPaciente(nome);
            return pacientes.ToArray();
        }

        [WebMethod]
        public static string[] PesquisarProcedimento(string nome)
        {
            string[] procedimentos = new ProcedimentoController().Pesquisar(nome);
            return procedimentos.ToArray();
        }

        protected void gdvOrcamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarGridOrcamentos(e.NewPageIndex);
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            OrcamentoController cOrc = new OrcamentoController();   
            int retorno = cOrc.Excluir(cod);
            string msg = "";
            switch (retorno)
            {
                case 1:
                    msg = "O orçamento foi excluído com sucesso.";
                    CarregarGridOrcamentos(0);
                    break;
                case -1:
                    msg = "Não foi possível excluir o orçamento selecionado.";
                    break;
            }
            MensagemJS(msg);
        }

        private void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void btnAprovar_Command(object sender, CommandEventArgs e)
        {
            string msg = "";
            int codOrc = Convert.ToInt32(e.CommandArgument);
            int codRetorno = new OrcamentoController().AprovarOrcamento(codOrc);
            if(codRetorno > 0)
            {
                msg = "Orçamento aprovado com sucesso.";
            }
            else
            {
                if (codRetorno == -10)
                {
                    msg = "Selecione o orçamento a ser aprovado.";
                }
                if (codRetorno == -5)
                {
                    msg = "Orçamento não encontrado na base de dados.";
                }
                if (codRetorno == -1)
                {
                    msg = "Erro na aplicação, contate o administrador do sistema.";
                }
            }
            MensagemJS(msg);
            CarregarGridOrcamentos(0);
        }

        protected void btnEditar_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            codOrcamento.Value = cod.ToString();
            OrcamentoViewModel vmOrc = new OrcamentoController().ObterPorCod(cod);
            txtDataOrcamento.Value = vmOrc.DtInicio.ToString();
            txtDentista.Value = vmOrc.NomeDentista; codDentista.Value = vmOrc.DentistaCod.ToString();
            txtDesconto.Value = vmOrc.Desconto.ToString();
            txtDescricao.Value = "";//ImplementaR
            txtEntrada.Value = vmOrc.ValorEntrada.ToString();
            txtPaciente.Value = vmOrc.NomePaciente; codPaciente.Value = vmOrc.PacienteCod.ToString();
            ItensOrc.ProcedimentoNome = vmOrc.Procedimento;
            txtQtdParcelas.Value = vmOrc.Parcelas.ToString();
            //ItensOrc.Valor = vmOrc.Val
            //txtValor.Value
            AtivarBotaoSalvar();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        protected void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }
        protected void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
            AtivarBotaoCadastrar();
            LimparCampos();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            OrcamentoController cOrc = new OrcamentoController();
            decimal valorTotal = 0;
            foreach (GridViewRow row in gdvOrcamentos.Rows)
            {
                //incrementa valor total
            }

            OrcamentoViewModel orc = new OrcamentoViewModel
            {
                Cod = Convert.ToInt32(codOrcamento),
                DentistaCod = Convert.ToInt32(codDentista),
                Desconto = Convert.ToInt32(txtDesconto.Value),
                DtInicio = Convert.ToDateTime(txtDataOrcamento),
                FuncionarioCod = Convert.ToInt32(Session["codFuncionario"]),
                ItensOrcamento = _itensOrc,
                NomeDentista = txtDentista.Value,
                PacienteCod = Convert.ToInt32(codPaciente.Value),
                Parcelas = Convert.ToInt32(txtQtdParcelas.Value),
                Status = Convert.ToInt32(ddlStatus.SelectedValue),
                Tipo = ddlTipo.SelectedItem.Text,
                ValorTotal = Convert.ToDecimal(valorTotal),
                ValorEntrada = Convert.ToDecimal(txtEntrada.Value)
            };

            int codRetorno = cOrc.Editar(orc);
            if(codRetorno > 0)
            {
                MensagemJS("O orçamento foi editado com sucesso.");
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Preencha os campos corretamente.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação, contate o administrador do sistema.");
                }
            }

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            if (Session["itensOrcamento"] != null)
                _itensOrc = (List<ItensOrcamentoViewModel>)Session["itensOrcamento"];
            decimal valorTotal = 0;
            foreach (GridViewRow row in gdvOrcamentos.Rows)
            {
                //incrementa valor total
            }
            
            OrcamentoViewModel orc = new OrcamentoViewModel
            {
                Cod = Convert.ToInt32(codOrcamento),
                DentistaCod = Convert.ToInt32(codDentista),
                Desconto = Convert.ToInt32(txtDesconto.Value),
                DtInicio = Convert.ToDateTime(txtDataOrcamento),
                FuncionarioCod = Convert.ToInt32(Session["codFuncionario"]),
                ItensOrcamento = _itensOrc,
                NomeDentista = txtDentista.Value,
                PacienteCod = Convert.ToInt32(codPaciente.Value),
                Parcelas = Convert.ToInt32(txtQtdParcelas.Value),
                Status = Convert.ToInt32(ddlStatus.SelectedValue),
                Tipo = ddlTipo.SelectedItem.Text,
                ValorTotal = valorTotal,
                ValorEntrada = Convert.ToDecimal(txtEntrada.Value)
            };

            int codRetorno = new OrcamentoController().Gravar(orc);
            if(codRetorno > 0)
            {
                //Sucesso
                MensagemJS("Orçamento cadastrado com sucesso.");
                LimparCampos();
                divLabel1.Visible = true;
                mensagemUsuario.InnerText = "O orçamento foi cadastrado com sucesso.";
                divAlert.Attributes.Add("class", "alert bg-green");
            }
            else
            {
                if(codRetorno == -10)
                {
                    divLabel1.Visible = true;
                    mensagemUsuario.InnerText = "Erro! Preencha os campos obrigatórios.";
                    divAlert.Attributes.Add("class", "alert bg-orange");
                }
                if(codRetorno == -1)
                {
                    divLabel1.Visible = true;
                    mensagemUsuario.InnerText = "Erro na aplicaçãos! contate o administrador do sistema.";
                    divAlert.Attributes.Add("class", "alert bg-red");
                }
            }

        }

        

        protected void LimparCampos()
        {
            txtDataOrcamento.Value = "";
            txtDentista.Value = "";
            txtDesconto.Value = "";
            txtDescricao.Value = "";
            txtEntrada.Value = "";
            txtPaciente.Value = "";            
            txtQtdParcelas.Value = "";           
            codProcedimento.Value = null;
            codPaciente.Value = null;
            codDentista.Value = null;
            codOrcamento.Value = null;
        }

        protected void CarregarGridOrcamentos(int pageIndex)
        {
            gdvOrcamentos.PageIndex = pageIndex;
            gdvOrcamentos.DataSource = new OrcamentoController().Obter();
            gdvOrcamentos.DataBind();
        }

        
    }
}