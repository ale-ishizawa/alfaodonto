﻿using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class ItensOrc : System.Web.UI.UserControl
    {
        List<ItensOrcamentoViewModel> _itensOrc = new List<ItensOrcamentoViewModel>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _itensOrc = null;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Session["itensOrcamento"] != null)
                {
                    _itensOrc = (List<ItensOrcamentoViewModel>)Session["itensOrcamento"];
                }
                ItensOrcamentoViewModel item = new ItensOrcamentoViewModel
                {
                    DenteRegiao = txtDenteRegiao.Value,
                    ItemQtd = 1,
                    ItemValor = Convert.ToDecimal(txtValor.Value),
                    OrcamentoCod = _itensOrc.Count,
                    // ProcedimentoCod = Convert.ToInt32(codProcedimento.Value),
                    ProcedimentoNome = txtProcedimento.Value

                };
                _itensOrc.Add(item);

                Session.Add("itensOrcamento", _itensOrc);
                gdvItens.DataSource = _itensOrc;
                gdvItens.DataBind();
                LimparCampos();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);

            }
        }

        protected void btnItemExcluir_Command(object sender, CommandEventArgs e)
        {
            _itensOrc = (List<ItensOrcamentoViewModel>)Session["itensOrcamento"];
            _itensOrc.RemoveAt(Convert.ToInt32(e.CommandArgument));
            int i = 0;
            foreach (ItensOrcamentoViewModel item in _itensOrc)
            {
                item.OrcamentoCod = i;
                i++;
            }
            Session.Add("itensOrcamento", _itensOrc);
            gdvItens.DataSource = _itensOrc;
            gdvItens.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }


        public string DenteRegiao
        {
            get
            {
                return txtDenteRegiao.Value;
            }
            set
            {
                txtDenteRegiao.Value = value;
            }
        }

        public decimal Valor
        {
            get
            {
                return Convert.ToDecimal(txtValor.Value);
            }
            set
            {
                txtValor.Value = value.ToString();
            }
        }

        public string ProcedimentoNome
        {
            get
            {
                return txtProcedimento.Value;
            }
            set
            {
                txtProcedimento.Value = value;
            }
        }

        public List<ItensOrcamentoViewModel> Itens
        {
            get
            {
                return _itensOrc;
            }
            set
            {
                Itens = value;
            }
        }
        protected void LimparCampos()
        {
            txtProcedimento.Value = "";
            txtDenteRegiao.Value = "";
            txtValor.Value = "";
        }
    }
}