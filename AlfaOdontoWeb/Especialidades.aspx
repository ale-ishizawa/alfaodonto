﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Especialidades.aspx.cs" Inherits="AlfaOdontoWeb.Especialidades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
     <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ESPECIALIDADES</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Nova Especialidade
                                <small></small>
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtDescricao"/>
                                                <label class="form-label">Descrição</label>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Button Text="CADASTRAR" runat="server" ID="btnCadastrar" CssClass="btn bg-green btn-lg m-l-15 waves-effect" OnClick="btnCadastrar_Click" />
                                          </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Inline Layout | With Floating Label -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Especialidades Cadastradas
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <asp:GridView runat="server" CssClass="table table-striped" AutoGenerateColumns="False" ID="gdvEspecialidades" GridLines="None" PageSize="10"
                                 OnPageIndexChanging="gdvEspecialidades_PageIndexChanging" AllowPaging="true" >
                                <Columns>
                                    <asp:BoundField DataField="Cod" HeaderText="Cod" />
                                    <asp:BoundField DataField="Descricao" HeaderText="Descricao" />
                                    
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnExcluir" OnClientClick="return confirm(&quot;Confirma a exclusão da especialidade?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnExcluir_Command" >
                                            </asp:Button>
                                        </ItemTemplate>
                                        <ControlStyle CssClass="btn bg-red waves-effect" />
                                    </asp:TemplateField>                          
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
            </div>
        </section>
    
</asp:Content>
