﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Dentistas.aspx.cs" Inherits="AlfaOdontoWeb.Dentistas" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <!-- Colorpicker Css -->
    <link href="../../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="../../plugins/dropzone/dropzone.css" rel="stylesheet"/>

    <!-- Multi Select Css -->
    <link href="../../plugins/multi-select/css/multi-select.css" rel="stylesheet"/>

    <!-- Bootstrap Spinner Css -->
    <link href="../../plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet"/>

    <!-- Bootstrap Tagsinput Css -->
    <link href="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"/>

    <!-- noUISlider Css -->
    <link href="../../plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
     <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function openModal() {
            $('#largeModal').modal('show');
        };
        function closeModal() {
            $('#largeModal').modal('close');
        };

        $(function () {
        $("[id$=txtNome]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Dentistas.aspx/PesquisarDentista") %>',
                    data: "{ 'nome': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=codDentista]").val(i.item.val);
                openModal();
            },
            minLength: 1
        });
    });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DENTISTAS</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Pesquisar Dentista ou Cadastrar Novo
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body blue">                            
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label for="txtNome">Buscar Dentista</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNome"/>                                                
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                                        <br />OU                                  
                                    </div>                                   
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br />
                                        <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">CADASTRAR NOVO DENTISTA</button>    
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           A especialidade foi cadastrada com sucesso.
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    <!-- Large Size MODAL -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <input type="hidden" id="codDentista" name="codDentista" runat="server" />
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Dados do Dentista</h4>
                        </div>
                        <div class="modal-body">
                            <div class="demo-masked-input">
                            <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="txtDentista">Nome*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtDentista" required="required"/>                                                
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="txtEmail">Email*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" class="form-control" runat="server" id="txtEmail" required="required"/>                                                
                                            </div>
                                        </div>
                                    </div>                              
                            </div> <!-- End Row -->
                            <div class="row clearfix">     
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtDtNascto">Data de Nascimento*</label>
                                       <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control date" runat="server" id="txtDtNascto" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtCpf">CPF*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control cpf" runat="server" id="txtCpf" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtRg">RG*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control rg" runat="server" id="txtRg" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtOrgaoEmissor">Órgão Emissor*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtOrgaoEmissor" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                            </div> <!-- End Row -->
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtCro">CRO*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtCro" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtSiglaConselho">Sigla Conselho*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtSiglaConselho" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtUfConselho">UF Conselho*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtUfConselho" required="required" maxlength="2" />                                                
                                            </div>
                                        </div>
                                 </div>                                
                            </div> <!-- End Row -->
                            <div class="row clearfix">   
                                 <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                     <label for="txtCelular">Celular*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control mobile-phone-number" runat="server" id="txtCelular" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtTelefoneResidencia">Telefone Residencial</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control phone-number" runat="server" id="txtTelefoneResidencia" />                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                    <label for="txtTelefoneComercial">Telefone Comercial</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control phone-number" runat="server" id="txtTelefoneComercial" />                                                
                                            </div>
                                        </div>
                                 </div>                                
                            </div> <!-- End Row -->                            
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">INFORMAÇÕES ADICIONAIS</a></li>
                                        <li role="presentation"><a href="#settings_animation_1" data-toggle="tab">ENDEREÇO</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab">DADOS DE ACESSO</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                          <div class="row clearfix">                                            
                                             <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                <label for="ddlTempoConsulta">Tempo de consulta*</label>
                                                <div class="form-group"> 
                                                <div class="form-line">
                                                    <asp:DropDownList runat="server" ID="ddlTempoConsulta" CssClass="form-control show-tick">
                                                            <asp:ListItem Text="--Tempo de Consulta--" Value="0"/>
                                                            <asp:ListItem Text="10" Value="10"/>
                                                            <asp:ListItem Text="20" Value="20"/>
                                                            <asp:ListItem Text="30" Value="30"/>
                                                            <asp:ListItem Text="40" Value="40"/>
                                                            <asp:ListItem Text="50" Value="50"/>
                                                            <asp:ListItem Text="60" Value="60"/>
                                                        </asp:DropDownList>                                                 
                                                   
                                                </div>
                                              </div>
                                             </div>
                                            <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                <label for="txtComissao">Comissão %*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" runat="server" id="txtComissao" required="required"/>                                                    
                                                </div>
                                              </div>
                                             </div>
                                           </div>                                                 
                                        </div>                                        
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                          <div class="row clearfix">
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                 <label for="txtCep">CEP*</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control cep" runat="server" id="txtCep" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                              </div>    
                                              <div class="col-lg-8 col-md-8 col-sm8 col-xs-8">
                                                  <label for="txtLogradouro">Logradouro</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtLogradouro" required="required"/>                                                      
                                                     </div>
                                                 </div>                                                 
                                             </div>      
                                          </div> <!-- End Row-->  
                                          <div class="row clearfix">
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <label for="txtNumero">Número</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtNumero" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div> 
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <label for="txtComplemento">Complemento</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtComplemento" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <label for="txtBairro">Complemento</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtBairro" required="required"/>
                                                     </div>
                                                 </div>                                                 
                                             </div>   
                                          </div> <!-- End Row -->
                                          <div class="row clearfix">
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <label for="txtCidade">Cidade</label>
                                                  <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtCidade" required="required"/>                                                        
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <label for="ddlEstados">Estado</label>                                              
                                                  <asp:DropDownList runat="server" ID="ddlEstados" CssClass="form-control show-tick">
                                                      <asp:ListItem Text="--Estado--" Value="0" /> 
                                                      <asp:ListItem Text="SP" Value="SP" />                                                      
                                                  </asp:DropDownList>                                                                            
                                             </div>  
                                          </div>
                                       </div>  
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">                                           
                                          <div class="row clearfix">                                            
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                 <label for="txtLogin">Login*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtLogin"/>                                 
                                                </div>
                                              </div>
                                             </div>
                                            <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <label for="txtSenha">Senha*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" runat="server" id="txtSenha" required="required"/>                                                    
                                                </div>
                                              </div>
                                             </div>
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <div class="form-group">
                                                 <br />
                                                    <div class="switch panel-switch-btn">
                                                        <span class="m-r-10 font-12">Ativo?</span>
                                                        <label>Não<input type="checkbox" id="ckbAtivo" checked="checked" runat="server"/><span class="lever switch-col-cyan"></span>Sim</label>
                                                    </div>                                                    
                                              </div>
                                             </div>
                                           </div>  <!-- End Row -->                                               
                                        </div>                                     
                                    </div>
                                </div>                                
                            </div> <!-- #END# Tabs With Custom Animations -->
                        </div>
                       </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" runat="server" ID="btnCadastrar" CssClass="btn bg-green waves-effect" OnClick="btnCadastrar_Click" />
                            <asp:Button Text="SALVAR" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" Visible="false" Enabled="false" />                            
                            <asp:Button Text="FECHAR" CssClass="btn btn-link waves-effect" runat="server" ID="btnFechar" OnClick="btnFechar_Click" formnovalidate="formnovalidate" />
                        </div>
                    </div>
                </div>
            </div>

    <!-- Bootstrap Colorpicker Js -->
    <script src="../../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="../../plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/forms/advanced-form-elements.js"></script>
</asp:Content>
