﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SweetAlert.aspx.cs" Inherits="AlfaOdontoWeb.SweetAlert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="../../plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../css/themes/all-themes.css" rel="stylesheet" />

    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="body">
                            <div class="row clearfix js-sweetalert">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A basic message</p>
                                    <button class="btn btn-primary waves-effect" data-type="basic">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A title with a text under</p>
                                    <button class="btn btn-primary waves-effect" data-type="with-title">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A success message!</p>
                                    <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A warning message, with a function attached to the <b>Confirm</b> button...</p>
                                    <button class="btn btn-primary waves-effect" data-type="confirm">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>... and by passing a parameter, you can execute something else for <b>Cancel</b>.</p>
                                    <button class="btn btn-primary waves-effect" data-type="cancel">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A message with a custom icon</p>
                                    <button class="btn btn-primary waves-effect" data-type="with-custom-icon">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>An HTML message</p>
                                    <button class="btn btn-primary waves-effect" data-type="html-message">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A message with auto close timer</p>
                                    <button class="btn btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>A replacement for the <b>prompt</b> function</p>
                                    <button class="btn btn-primary waves-effect" data-type="prompt">CLICK ME</button>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>With a loader (for AJAX request for example)</p>
                                    <button class="btn btn-primary waves-effect" data-type="ajax-loader">CLICK ME</button>
                                </div>
                            </div>
        </div>
        
    </div>
    </form>
     <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="../../plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/dialogs.js"></script>

    <!-- Demo Js -->
    <script src="../../js/demo.js"></script>


</body>
</html>
