﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Planos.aspx.cs" Inherits="AlfaOdontoWeb.Planos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Colorpicker Css -->
    <link href="../../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="../../plugins/dropzone/dropzone.css" rel="stylesheet"/>

    <!-- Multi Select Css -->
    <link href="../../plugins/multi-select/css/multi-select.css" rel="stylesheet"/>

    <!-- Bootstrap Spinner Css -->
    <link href="../../plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet"/>

    <!-- Bootstrap Tagsinput Css -->
    <link href="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"/>

    <!-- noUISlider Css -->
    <link href="../../plugins/nouislider/nouislider.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PLANOS</h2>
            </div>
            <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Novo Plano
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                       <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">NOVO PLANO</button> 
                                     </div>                                   
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Default Size -->
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Novo Plano</h4>
                        </div>
                        <div class="modal-body">
                            <div class="demo-masked-input">
                           <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label for="txtNomePlano">Plano*</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNomePlano" required="required"/>                                                
                                            </div>
                                     </div>
                                </div>
                            </div> <!-- Fim da Linha-->
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="txtValor">Valor</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control money-dollar" runat="server" id="txtValor" required="required"/>
                                            </div>
                                     </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                           <div class="switch panel-switch-btn">
                                                 <span class="m-r-10 font-12">Padrão?</span>
                                                 <label>Não<input type="checkbox" id="ckbPadrao" checked="checked" runat="server"/><span class="lever switch-col-cyan"></span>Sim</label>
                                           </div>   
                                     </div>
                                </div>
                            </div> <!-- Fim da Linha-->
                        </div>
                            </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" ID="btnCadastrar" CssClass="btn btn-link waves-effect" runat="server" OnClick="btnCadastrar_Click" />                            
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FECHAR</button>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
         <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
    <!-- Bootstrap Colorpicker Js -->
    <script src="../../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
     <!-- Input Mask Plugin Js -->
    <script src="../../plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>   
    
    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/forms/advanced-form-elements.js"></script>
</asp:Content>
