﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Pacientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        [WebMethod]
        public static string[] PesquisarPaciente(string nome)
        {
            string[] pacientes = new PacienteController().PesquisarPaciente(nome);
            return pacientes.ToArray();
        }
        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            PacienteController cPac = new PacienteController();

            string genero = "";
            if (radio_masculino.Checked)
                genero = "M";
            else
                genero = "F";
            
            PacienteViewModel vmPac = new PacienteViewModel
            {
                Cod = 0,
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Complemento = txtComplemento.Value,
                Cpf = txtCpf.Value,
                DiaVencto = Convert.ToInt32(txtDiaVencto.Value),
                DtCadastro = DateTime.Now,
                DtNascto = Convert.ToDateTime(txtDtNascto.Value),
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedItem.Text,
                EstadoCivil = ddlEstadoCivil.SelectedItem.Text,
                Genero = genero,
                Logradouro = txtLogradouro.Value,
                Nome = txtNome.Value,
                NomeRecado = txtNomeRecado.Value,
                Numero = txtNumero.Value,
                OrgaoEmissor = txtOrgaoEmissor.Value,
                PlanoCod = Convert.ToInt32(ddlPlanos.SelectedValue),
                Rg = txtRg.Value,
                TelCelular = txtCelular.Value,
                TelComercial = txtTelefoneComercial.Value,
                TelRecado = txtTelefoneRecado.Value,
                TelResidencia = txtTelefoneResidencia.Value
            };

            UsuarioViewModel usu = new UsuarioViewModel
            {
                Ativo = 1,
                Login = txtLogin.Value,
                Senha = txtSenha.Value,
                Tipo = "P",
                Paciente = vmPac
            };

            int codRetorno = cPac.Gravar(usu);
            if(codRetorno > 0)
            {
                //SUCESSO
                MensagemJS("Cadastrado com sucesso.");
            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    //Preencha os campos obrigatórios.
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação, contate o administrador do sistema.");
                }
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            string genero;
            if (radio_masculino.Checked)
                genero = "M";
            else
                genero = "F";
            PacienteController cPac = new PacienteController();
            PacienteViewModel vmPac = new PacienteViewModel
            {
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Cod = Convert.ToInt32(codPaciente),
                Complemento = txtComplemento.Value,
                Cpf = txtCpf.Value,
                DtNascto = Convert.ToDateTime(txtDtNascto.Value),
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedValue,
                Logradouro = txtLogradouro.Value,
                Nome = txtNome.Value,
                Numero = txtNumero.Value,
                Rg = txtRg.Value,
                TelCelular = txtCelular.Value,
                TelResidencia = txtTelefoneResidencia.Value,
                DiaVencto = Convert.ToInt32(txtDiaVencto.Value),
                DtCadastro = DateTime.Now.Date,
                EstadoCivil = ddlEstadoCivil.SelectedValue,
                NomeRecado = txtNomeRecado.Value,
                OrgaoEmissor = txtOrgaoEmissor.Value,
                PlanoCod = Convert.ToInt32(ddlPlanos.SelectedValue),
                TelComercial = txtTelefoneComercial.Value,
                TelRecado = txtTelefoneRecado.Value,
                Genero = genero
            };

            int codRetorno = cPac.Editar(vmPac);
            if(codRetorno > 0)
            {
                MensagemJS("Paciente editado com sucesso.");
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação, consulte o administrador do sistema.");
                }
            }
            AtivarBotaoCadastrar();
            codPaciente.Value = "";
            LimparCampos();
        }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }

        protected void LimparCampos()
        {
            txtBairro.Value = ""; txtCelular.Value = ""; txtCep.Value = "";
            txtCidade.Value = ""; txtComplemento.Value = ""; txtCpf.Value = "";
            txtDiaVencto.Value = ""; txtDtNascto.Value = "";
            txtEmail.Value = ""; txtLogin.Value = ""; txtLogradouro.Value = ""; 
            txtNome.Value = ""; txtNomeRecado.Value = ""; txtNumero.Value = "";
            txtOrgaoEmissor.Value = ""; txtPaciente.Value = ""; txtRg.Value = "";
            txtSenha.Value = ""; txtTelefoneComercial.Value = ""; txtTelefoneRecado.Value = "";
            txtTelefoneResidencia.Value = ""; ddlEstadoCivil.SelectedIndex = 1;
            ddlEstados.SelectedIndex = 1; ddlPlanos.SelectedIndex = 1;
            ckbAtivo.Checked = true; radio_masculino.Checked = true;

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
            AtivarBotaoCadastrar();
            LimparCampos();
            codPaciente.Value = "";
        }
    }
}