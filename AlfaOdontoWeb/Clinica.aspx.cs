﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Clinica : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            
            ClinicaController cCli = new ClinicaController();
            ClinicaViewModel vmCli = new ClinicaViewModel
            {
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                Cnpj = txtCnpj.Value,
                Cod = 0,
                Complemento = txtComplemento.Value,
                Email = txtEmail.Value,
                Estado = txtEstado.Value,
                Fax = txtFax.Value,
                Logradouro = txtLogradouro.Value,
                Numero = txtNumero.Value,
                Site = txtSite.Value,
                Nome = txtNomeClinica.Value,
                Celular = txtCelular.Value,
                Telefone = txtTelefone.Value
            };
            int codRetorno = cCli.Gravar(vmCli);

            if(codRetorno > 0)
            {
                //SUCESSO

            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    //Preencha os campos obrigatórios
                }
            }

        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {

        }

        protected void VerificarCadastro()
        {
            ClinicaViewModel vmCli = new ClinicaController().VerificarCadastro();
            if(vmCli == null)
            {
                //Ainda não possui cadastro da clínica

            }
            else
            {
                txtBairro.Value = vmCli.Bairro; txtBairro.Disabled = true;
                txtCelular.Value = vmCli.Celular; txtCelular.Disabled = true;
                txtCep.Value = vmCli.Cep; txtCep.Disabled = true;
                txtCidade.Value = vmCli.Cidade; txtCidade.Disabled = true;
                txtCnpj.Value = vmCli.Cnpj; txtCnpj.Disabled = true;
                txtComplemento.Value = vmCli.Complemento; txtComplemento.Disabled = true;
                txtEmail.Value = vmCli.Email; txtEmail.Disabled = true;
                txtEstado.Value = vmCli.Estado; txtEstado.Disabled = true;
                txtFax.Value = vmCli.Fax; txtFax.Disabled = true;
                txtIE.Value = vmCli.IE; txtIE.Disabled = true;
                txtLogradouro.Value = vmCli.Logradouro; txtLogradouro.Disabled = true;
                txtNomeClinica.Value = vmCli.Nome; txtNomeClinica.Disabled = true;
                txtNumero.Value = vmCli.Numero; txtNumero.Disabled = true;
                txtSite.Value = vmCli.Site; txtSite.Disabled = true;
                txtTelefone.Value = vmCli.Telefone; txtTelefone.Disabled = true;                
            }
        }
    }
}