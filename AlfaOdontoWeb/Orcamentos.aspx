﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Orcamentos.aspx.cs" Inherits="AlfaOdontoWeb.Orcamentos" %>

<%@ Register Src="~/ItensOrc.ascx" TagPrefix="uc1" TagName="ItensOrc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        //AutoCompleteDentista
      $(function () {
        $("[id$=txtDentista]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Orcamentos.aspx/PesquisarDentista") %>',
                    data: "{ 'nome': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        })) 
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=codDentista]").val(i.item.val);
            },
            minLength: 1
        });
      });

        //AutoComplete Paciente
         $(function () {
        $("[id$=txtPaciente]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Orcamentos.aspx/PesquisarPaciente") %>',
                    data: "{ 'nome': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=codPaciente]").val(i.item.val);
            },
            minLength: 1
        });
         });

        //AutoComplete Procedimento
         $(function () {
        $("[id$=txtProcedimento]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Orcamentos.aspx/PesquisarProcedimento") %>',
                    data: "{ 'nome': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=codProcedimento]").val(i.item.val);
            },
            minLength: 1
        });
         });

        function openModal() {
            $('#largeModal').modal('show');
        };
        function closeModal() {
            $('#largeModal').modal('close');
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ORÇAMENTOS</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Novo Orçamento
                                <small></small>
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                       <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">NOVO ORÇAMENTO</button> 
                                          </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div> <!-- FIm da Linha -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Orçamentos Cadastrados
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <asp:GridView runat="server" ID="gdvOrcamentos" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None"
                                PageSize="15" OnPageIndexChanging="gdvOrcamentos_PageIndexChanging"  AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="Cod" HeaderText="#" />
                                    <asp:BoundField DataField="Nome" HeaderText="Paciente" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="Nome" HeaderText="Paciente" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnAprovar" runat="server" CausesValidation="false" Text="Aprovar" CommandArgument='<%# Eval("Cod") %>'
                                                 OnCommand="btnAprovar_Command" CssClass="btn bg-green waves-effect" >
                                            </asp:Button>
                                            <asp:Button ID="btnEditar" runat="server" CausesValidation="false" Text="Editar" CommandArgument='<%# Eval("Cod") %>'
                                                 OnCommand="btnEditar_Command" CssClass="btn bg-blue waves-effect" >
                                            </asp:Button>
                                            <asp:Button ID="btnExcluir" OnClientClick="return confirm(&quot;Confirma a exclusão do orçamento?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnExcluir_Command"
                                                CssClass="btn bg-red waves-effect" >
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>                          
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
            </div>
            </section>
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <input type="hidden" id="codPaciente" runat="server" />
                <input type="hidden" id="codDentista" runat="server" />
                <input type="hidden" id="codProcedimento" runat="server" />
                <input type="hidden" id="codOrcamento" runat="server" />
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Orçamento</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <label for="txtDescricao">Descrição</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                               <input type="text" runat="server" id="txtDescricao" class="form-control" required />                                          
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <label for="txtDataOrcamento">Data do Orçamento</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                     <input type="date" id="txtDataOrcamento" runat="server" class="form-control" required />                                      
                                            </div>
                                        </div>
                                    </div>
                              </div>     
                              <div class="row clearfix">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="txtPaciente">Nome do Paciente</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                               <input type="text" runat="server" id="txtPaciente" class="form-control" required />                                          
                                            </div>
                                        </div>
                                    </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="txtDentista">Nome do Dentista</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                               <input type="text" runat="server" id="txtDentista" class="form-control" required />                                          
                                            </div>
                                        </div>
                                    </div>
                              </div>                             
                              <div class="row clearfix">     
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                       <div class="form-group">
                                            <div class="form-line">
                                                <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control" required>
                                                    <asp:ListItem Text="--Selecione o Tipo--" Value="0" />
                                                    <asp:ListItem Text="Manutenção" Value="1" />
                                                    <asp:ListItem Text="Tratamento Clínico" Value="2" />
                                                </asp:DropDownList>    
                                            </div>
                                        </div>
                                 </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                       <div class="form-group">
                                            <div class="form-line">
                                                <asp:DropDownList runat="server" ID="ddlStatus" CssClass="form-control" required>
                                                    <asp:ListItem Text="--Selecione o Status--" Value="0" />
                                                    <asp:ListItem Text="Aguardando Aprovação" Value="1" />
                                                    <asp:ListItem Text="Aprovado" Value="2" />
                                                </asp:DropDownList>    
                                            </div>
                                        </div>
                                 </div>
                              </div> <!-- Fim da Linha -->       
                            <!-- Web User Control -->                         
                            <uc1:ItensOrc runat="server" id="ItensOrc" />
                                <div class="row clearfix">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <label for="txtDesconto">Desconto R$</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="text" id="txtDesconto" class="form-control" runat="server" />
                                              </div>
                                          </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <label for="ckbParcelar">Parcelar?</label>
                                          <div class="form-group">
                                               <div class="switch"><br />
                                                   <label>Não<input id="ckbParcelar" runat="server" type="checkbox" checked/><span class="lever"/></span>Sim</label>
                                                </div>
                                          </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <label for="txtQtdParcelas">Em quantas parcelas?</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="text" id="txtQtdParcelas" class="form-control" runat="server" />
                                              </div>
                                          </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <label for="txtEntrada">Entrada R$</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="text" id="txtEntrada" class="form-control" runat="server" />
                                              </div>
                                          </div>
                                  </div>
                              </div> <!-- Fim da Linha -->
                              <div class="row clearfix text-right">
                                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8"></div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <label for="lblSubtotal">SubTotal</label>
                                          <div class="form-group">
                                                  <asp:Label Text="" ID="lblSubtotal" runat="server" />
                                          </div>
                                      <label for="lblDesconto" runat="server">Desconto</label>
                                          <div class="form-group">
                                                  <asp:Label Text="" ID="lblDesconto" runat="server" />
                                          </div>
                                      <label for="lblTotal" runat="server">Desconto</label>
                                          <div class="form-group">
                                                  <asp:Label Text="" ID="lblTotal" runat="server" />
                                          </div>
                                  </div>
                              </div>
                           </div>                               
                            <div class="modal-footer">
                                <asp:Button Text="CADASTRAR" CssClass="btn bg-green waves-effect" runat="server" ID="btnCadastrar" OnClick="btnCadastrar_Click" />
                                <asp:Button Text="SALVAR" CssClass="btn bg-green waves-effect" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" Enabled="false" Visible="false" />
                                <asp:Button Text="FECHAR" runat="server" ID="btnFechar" CssClass="btn btn-link" OnClick="btnFechar_Click" formnovalidate="formnovalidate"/>
                            </div>
                        </div> 
                       </div>
                    </div>
    <%--<script src="plugins/jquery/jquery.min.js"></script>--%>
                <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
      <!-- Input Mask Plugin Js -->
   <%-- <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>--%>
<%--     <script src="js/pages/forms/advanced-form-elements.js"></script>--%>
    
    <!-- Multi Select Plugin Js -->
   <%-- <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>--%>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
   <%-- <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>--%>

    
    <!-- noUISlider Plugin Js -->
<%--    <script src="../../plugins/nouislider/nouislider.js"></script>--%>

</asp:Content>
