﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Funcionarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.VerificarAutenticacao();
            if (!IsPostBack)
            {
                
            }
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            FuncionarioController cFun = new FuncionarioController();
            FuncionarioViewModel fun = new FuncionarioViewModel
            {
                Cod = 0,
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Complemento = txtComplemento.Value,
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedItem.Text,
                Logradouro = txtLogradouro.Value,
                Nome = txtFuncionario.Value,
                Numero = txtNumero.Value,
                TelCelular = txtCelular.Value,
                TelResidencia = txtTelefoneResidencia.Value
            };
            UsuarioViewModel usu = new UsuarioViewModel
            {
                Ativo = 1,
                Login = txtLogin.Value,
                Senha = txtSenha.Value,
                Tipo = "F",
                Funcionario = fun
            };

            int codRetorno = cFun.Gravar(usu);
            if (codRetorno > 0)
            {
                //SUCESSO
            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    //Preencha os campos obrigatórios.
                }
            }
        }
        
        protected void btnFechar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
            AtivarBotaoCadastrar();
            LimparCampos();
            codFuncionario.Value = "";
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            FuncionarioController cFun = new FuncionarioController();
            FuncionarioViewModel vmFun = new FuncionarioViewModel
            {
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Cod = Convert.ToInt32(codFuncionario.Value),
                Complemento = txtComplemento.Value,
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedValue,
                Grupo = "",
                Logradouro = txtLogradouro.Value,
                Nome = txtNome.Value,
                Numero = txtNumero.Value,
                TelCelular = txtCelular.Value,
                TelResidencia = txtTelefoneResidencia.Value
            };

            int codRetorno = cFun.Editar(vmFun);
            if (codRetorno > 0)
            {
                MensagemJS("Funcionário editado com sucesso.");
            }
            else
            {
                if (codRetorno == -10)
                {
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if (codRetorno == -1)
                {
                    MensagemJS("Não foi possível editar o funcionário.");
                }
            }
            AtivarBotaoCadastrar();
            codFuncionario.Value = "";
            LimparCampos();
        }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }
        protected void LimparCampos()
        {
            //Verificar se todos os campos estão sendo limpos
            txtBairro.Value = ""; txtCelular.Value = ""; txtCep.Value = "";
            txtCidade.Value = ""; txtComplemento.Value = ""; txtDtNascto.Value = "";
            txtEmail.Value = ""; txtFuncionario.Value = ""; txtLogin.Value = "";
            txtLogradouro.Value = ""; txtNome.Value = ""; txtNumero.Value = "";
            txtSenha.Value = ""; txtTelefoneResidencia.Value = ""; ddlEstados.SelectedIndex = 0;
            ckbAtivo.Checked = true;
        }

    }
}