﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            UsuarioController cUsuario = new UsuarioController();
            UsuarioViewModel vmUsuario = cUsuario.Autenticar(txtNome.Value, txtSenha.Value);
            
            if (vmUsuario != null)
            {
                Session["autenticado"] = "OK";
                Session["Cod"] = vmUsuario.Cod;
                Session["Tipo"] = vmUsuario.Tipo;                
                Server.Transfer("Dashboard.aspx");
                
            }
            else
            {
                //ERRO
            }
        }
    }
}