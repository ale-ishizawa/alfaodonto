﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Procedimentos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.VerificarAutenticacao();
            if (!IsPostBack)
            {
                CarregarDropDownEspec();
                CarregarGridProcedimentos(0);
            }
        }
        private void CarregarDropDownEspec()
        {
            ddlEspecialidades.DataSource = new EspecialidadeController().ObterEspecialidades();
            ddlEspecialidades.DataTextField = "Descricao";
            ddlEspecialidades.DataValueField = "Cod";
            ddlEspecialidades.DataBind();
        }

        protected void gdvProcedimentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            ProcedimentoController cProc = new ProcedimentoController();
            ProcedimentoViewModel proc = new ProcedimentoViewModel();
            proc.Descricao = txtDescricao.Value;
            proc.EspecialidadeCod = Convert.ToInt32(ddlEspecialidades.SelectedValue);
            if(cProc.Gravar(proc) > 0)
            {
                //SUCESSO
                ddlEspecialidades.SelectedIndex = 0;
                txtDescricao.Value = "";
                MensagemJS("Procedimento cadastrado com sucesso!");
                CarregarGridProcedimentos(0);
                //MensagemSucesso("Procedimento cadastrado com sucesso!");
                //CarregarGridProcedimentos();
            }
            else
            {
                ddlEspecialidades.SelectedIndex = 0;
                txtDescricao.Value = "";
                //ERRO
                MensagemJS("Erro ao cadastrar um procedimento.");
            }
        }
        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void MensagemSucesso(string mensagem)
        {
            string script = "$.notify({ message: '"+mensagem+"' },{ type: 'success' }); )";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemSucesso", script, true);
        }

        private void CarregarGridProcedimentos(int pageIndex)
        {
            gdvProcedimentos.PageIndex = pageIndex;
            gdvProcedimentos.DataSource = new ProcedimentoController().ObterProcedimentos();
            gdvProcedimentos.DataBind();
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            ProcedimentoController cPro = new ProcedimentoController();
            int retorno = cPro.Excluir(cod);
            string msg = "";
            switch (retorno)
            {
                case 1:
                    msg = "O procedimento foi excluído com sucesso.";
                    CarregarGridProcedimentos(0);
                    break;
                case -1:
                    msg = "Não foi possível excluir o procedimento selecionado.";
                    break;
                case -5:
                    msg = "Dados inválidos";
                    break;
            }
            MensagemJS(msg);
        }

        protected void btnEditar_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            codProcedimento.Value = cod.ToString();
            ProcedimentoViewModel vmPro = new ProcedimentoController().ObterPorCod(cod);
            txtDescricao.Value = vmPro.Descricao;
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }
    }
}