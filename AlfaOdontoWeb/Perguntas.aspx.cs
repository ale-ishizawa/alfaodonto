﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Perguntas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarPerguntas(0);
            }
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            PerguntaController cPer = new PerguntaController();
            string alerta = "";
            if (ckbAlerta.Checked)
                alerta = "S";
            else
                alerta = "N";
            PerguntaViewModel vmPer = new PerguntaViewModel
            {
                Descricao = txtDescricao.Value,
                Ativa = 1,
                Cod = 0,
                Alerta = alerta,
                Tipo = ddlTipoPergunta.SelectedItem.Text,
                AlertaDescricao = txtNomeAlerta.Value,
                AlertaQuando = ddlAlertaAnamnese.SelectedValue
            };
            int codRetorno = cPer.Gravar(vmPer);
            if(codRetorno > 0)
            {
                //SUCESSO
                LimparCampos();
                divLabel1.Visible = true;
                mensagemUsuario.InnerText = "A pergunta foi cadastrada com sucesso.";
                divAlert.Attributes.Add("class", "alert bg-green");
                CarregarPerguntas(0);

            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    divLabel1.Visible = true;
                    mensagemUsuario.InnerText = "Preencha todos os campos obrigatórios.";
                    divAlert.Attributes.Add("class", "alert bg-red");
                }
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {

        }

        protected void LimparCampos()
        {
            txtDescricao.Value = "";
            txtNomeAlerta.Value = "";
            txtDescricao.Value = "";
            ddlAlertaAnamnese.SelectedIndex = 1;
            ddlTipoPergunta.SelectedIndex = 0;
            ckbAlerta.Checked = false;
            divLabel1.Visible = false;
            divAlert.Attributes.Remove("class");
            mensagemUsuario.InnerText = "";
        }

        protected void gdvPerguntas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarPerguntas(e.NewPageIndex);
        }

        protected void CarregarPerguntas(int pageIndex)
        {
            gdvPerguntas.PageIndex = pageIndex;
            gdvPerguntas.DataSource = new PerguntaController().ObterPerguntas();
            gdvPerguntas.DataBind();
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            string msg = "";
            int cod = Convert.ToInt32(e.CommandArgument);
            PerguntaController cPer = new PerguntaController();
            int codRetorno = cPer.Excluir(cod);
            switch (codRetorno)
            {
                case 1:
                    msg = "A pergunta foi excluida com sucesso.";
                    break;
                case -1:
                    msg = "Erro na aplicação, consulte o administrador do sistema";
                    break;
                case -10:
                    msg = "Selecione a pergunta a ser excluída.";
                    break;                
            }
            MensagemJS(msg);
            CarregarPerguntas(0);
   }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void btnEditar_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            codPergunta.Value = cod.ToString();
            PerguntaViewModel vmPerg = new PerguntaController().ObterPorCod(cod);
            txtDescricao.Value = vmPerg.Descricao;
            txtNomeAlerta.Value = vmPerg.AlertaDescricao;
            ddlTipoPergunta.SelectedIndex = vmPerg.Tipo; ddlTipoPergunta.SelectedValue = vmPerg.Tipo.ToString();
            //Ver regra para carregar drop down ddlAlertaAnamnese.SelectedValue = vmPerg.AlertaQuando;
            AtivarBotaoSalvar();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            int alerta;
            PerguntaController cPerg = new PerguntaController();
            if (ckbAlerta.Checked)
                alerta = 1;
            else
                alerta = 2;
            PerguntaViewModel vmPerg = new PerguntaViewModel
            {
                Cod = Convert.ToInt32(codPergunta.Value),
                Alerta = alerta,
                AlertaDescricao = txtNomeAlerta.Value,
                AlertaQuando = ddlAlertaAnamnese.SelectedValue,
                Ativa = 1,
                Descricao = txtDescricao.Value,
                Tipo = Convert.ToInt32(ddlTipoPergunta.SelectedValue) 
            };

            int codRetorno = cPerg.Editar(vmPerg);
            if(codRetorno > 0)
            {
                MensagemJS("A pergunta foi editada com sucesso.");
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Não foi possível editar a pergunta, consulte o administrador do sistema.");
                }
            }
            AtivarBotaoCadastrar();
            codPergunta.Value = "";
            CarregarPerguntas(0);
            LimparCampos();
        }

        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }
    }
}