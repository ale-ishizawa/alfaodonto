﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Sobre : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cod = Request.QueryString["cod"];    
                if(cod != null)
                {
                    //Carrega os dados do Paciente
                    int id;
                    if(int.TryParse(cod, out id))
                    {
                        PacienteViewModel vmPac = new PacienteController().Obter(id);
                        if(vmPac != null)
                        {
                            ltlBairro.Text = vmPac.Bairro;
                            ltlCelular.Text = vmPac.TelCelular;
                            ltlCep.Text = vmPac.Cep;
                            ltlCidade.Text = vmPac.Cidade;
                            ltlCpf.Text = vmPac.Cpf;
                            ltlDataNascto.Text = vmPac.DtNascto.ToString();
                            ltlEndereco.Text = vmPac.Logradouro;
                            ltlIdade.Text = "Calcular";
                            ltlNomePaciente.Text = vmPac.Nome;
                            ltlNumeroPaciente.Text = vmPac.Cod.ToString();
                            ltlPlano.Text = vmPac.PlanoCod.ToString();
                            ltlRg.Text = vmPac.Rg;
                            ltlSexo.Text = vmPac.Genero;
                            ltlUf.Text = vmPac.Estado;
                        }
                    }
                }
            }
        }
    }
}