﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Perguntas.aspx.cs" Inherits="AlfaOdontoWeb.Perguntas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript">
           function openModal() {
               $('#defaultModal').modal('show');
           };
           function closeModal() {
               $('#defaultModal').modal('close');
           };

       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
        function ShowHideDiv(chkPassport) {
            var dvPassport = document.getElementById("rowAlerta");
            dvPassport.style.display = chkPassport.checked ? "block" : "none";
            //Tornando o campo txtNomeAlerta required apenas se o Ckb estiver checked
            var nomeAlerta = document.getElementById("txtNomeAlerta");
            if(chkPassport.checked){
                nomeAlerta.setAttribute("required", "");
            } else {
                nomeAlerta.removeAttribute("required");
            }
             
                
        }
    </script>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PERGUNTAS</h2>
            </div>
            <input type="hidden" id="codPergunta" name="codPergunta" runat="server" />
            <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Nova Pergunta
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                       <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">NOVA PERGUNTA</button> 
                                     </div>                                   
                                </div>   
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                </div>                         
                        </div>
                    </div>
                </div>
            </div> <!--Fim da Linha -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Especialidades Cadastradas
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <asp:GridView runat="server" CssClass="table table-striped" AutoGenerateColumns="False" ID="gdvPerguntas" GridLines="None" PageSize="10"
                                 OnPageIndexChanging="gdvPerguntas_PageIndexChanging" AllowPaging="true" >
                                <Columns>
                                    <asp:BoundField DataField="Descricao" HeaderText="Descrição" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="Alerta" HeaderText="É um Alerta?" />
                                    <asp:BoundField DataField="Ativa" HeaderText="Ativa?" />
                                    <asp:BoundField DataField="Cod" Visible="false" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnExcluir" CssClass="btn bg-red waves-effect"  OnClientClick="return confirm(&quot;Confirma a exclusão do produto?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' formnovalidate="formnovalidate" OnCommand="btnExcluir_Command" >
                                            </asp:Button>
                                            <asp:Button ID="btnEditar" runat="server" CausesValidation="false" CssClass="btn bg-blue waves-effect" Text="Editar" CommandArgument='<%# Eval("Cod") %>'
                                                  formnovalidate="formnovalidate" OnCommand="btnEditar_Command" >
                                            </asp:Button>
                                            <label>
                                                <asp:CheckBox Text="" runat="server" ID="ckbAtiva" CssClass="" /><span class="lever switch-col-blue"></span></label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
        </div>
    </section>
    <!-- Large Size MODAL -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Nova Pergunta</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtDescricao" required="required"/>
                                                <label class="form-label">Descricão*</label>
                                            </div>
                                     </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                            <div class="form-line">
                                                <asp:DropDownList runat="server" ID="ddlTipoPergunta" CssClass="form-control show-tick" required="required">
                                                    <asp:ListItem Text="--Tipo de Pergunta--" Value="0"/>
                                                    <asp:ListItem Text="Sim/Não/Não Sei" Value="1"/>
                                                    <asp:ListItem Text="Sim/Não/Não Sei e Texto" Value="2" />
                                                    <asp:ListItem Text="Somente Texto" Value="3" />
                                                    <asp:ListItem Text="Esquerda/Direita/Não Sei" Value="4" />
                                                </asp:DropDownList>
                                            </div>
                                     </div>
                                </div>
                            </div> <!-- Fim da Linha-->
                            <div class="row clearfix demo-checkbox">
                                <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                   <div class="demo-switch-title"><b>É um alerta?</b></div>
                                        <div class="switch">
                                            <label>
                                                <input type="checkbox" id="ckbAlerta" onclick="ShowHideDiv(this)" runat="server" /><span class="lever switch-col-blue"></span></label>
                                     </div>
                                </div>
                            </div>
                            <div class="row clearfix" id="rowAlerta" style="display: none">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                            <div class="form-line">
                                                <asp:DropDownList runat="server" ID="ddlAlertaAnamnese" CssClass="form-control show-tick">
                                                    <asp:ListItem Text="É um alerta quando responder sim" Value="S"/>
                                                    <asp:ListItem Text="É um alerta quando responder não"  Value="N"/>
                                                </asp:DropDownList>
                                            </div>
                                     </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNomeAlerta"/>
                                                <label class="form-label">Nome do Alerta*</label>
                                            </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" ID="btnCadastrar" OnClick="btnCadastrar_Click" CssClass="btn bg-green waves-effect" runat="server" />
                            <asp:Button Text="SALVAR" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" Visible="false" Enabled="false" />
                            <asp:Button Text="FECHAR" CssClass="btn btn-link waves-effect" runat="server" ID="btnFechar" data-dismiss="modal" OnClick="btnFechar_Click" />                          
                        </div>
                     </div>
                </div>
            </div>

         <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
      <!-- Input Mask Plugin Js -->
    <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
     <script src="js/pages/forms/advanced-form-elements.js"></script>
    
    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

</asp:Content>
