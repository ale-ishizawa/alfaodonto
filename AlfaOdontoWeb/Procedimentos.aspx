﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Procedimentos.aspx.cs" Inherits="AlfaOdontoWeb.Procedimentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
     <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript">
        function openModal() {
            $('#defaultModal').modal('show');
        };
        function openModalSmall() {
            $('#smallModal').modal('show');
        };
        function closeModal() {
            $('#defaultModal').modal('close');
        };
        function closeModalSmall() {
            $('#smallModal').modal('close');
        };
    </script>
     </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PROCEDIMENTOS</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Novo Procedimento
                                <small></small>
                            </h2>
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label for="ddlEspecialidades">Descrição</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:DropDownList runat="server" ID="ddlEspecialidades" CssClass="form-control">                                                    
                                                </asp:DropDownList>                                               
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label for="txtDescricao">Descrição</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtDescricao"/>                                                
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <br />
                                        <asp:Button Text="CADASTRAR" runat="server" ID="btnCadastrar" CssClass="btn bg-green btn-lg m-l-15 waves-effect" OnClick="btnCadastrar_Click" />
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                         <%--<div class="jsdemo-notification-button">
                                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                                            <button type="button" class="btn btn-success btn-block waves-effect" data-placement-from="bottom" data-placement-align="center"
                                                    data-animate-enter="" data-animate-exit="" data-color-name="alert-success"                                        
                                            </button>
                                            </div>
                                        </div>--%>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Inline Layout | With Floating Label -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>    
                                Procedimentos Cadastrados
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <input type="hidden" id="codProcedimento" name="codProcedimento" runat="server" />     
                            <asp:GridView runat="server" CssClass="table table-striped" AutoGenerateColumns="False" ID="gdvProcedimentos" GridLines="None" PageSize="10"
                                 OnPageIndexChanging="gdvProcedimentos_PageIndexChanging" AllowPaging="true" >
                                <Columns>
                                    <asp:BoundField DataField="Cod" HeaderText="Cod" />
                                    <asp:BoundField DataField="EspecialidadeDescricao" HeaderText="Especialidade" />
                                    <asp:BoundField DataField="Descricao" HeaderText="Descricao" />                                    
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnExcluir" OnClientClick="return confirm(&quot;Confirma a exclusão do procedimento?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnExcluir_Command" >
                                            </asp:Button>
                                            <asp:Button ID="btnEditar" runat="server" CausesValidation="false" CssClass="btn bg-blue waves-effect" Text="Editar" CommandArgument='<%# Eval("Cod") %>'
                                                  formnovalidate="formnovalidate" OnCommand="btnEditar_Command">
                                            </asp:Button>
                                        </ItemTemplate>
                                        <ControlStyle CssClass="btn bg-red waves-effect" />
                                    </asp:TemplateField>                          
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
            </div>
        </section>    
    <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/notifications.js"></script>
</asp:Content>

