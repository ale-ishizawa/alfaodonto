﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Anamneses.aspx.cs" Inherits="AlfaOdontoWeb.Anamneses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <!--WaitMe Css-->
    <link href="plugins/waitme/waitMe.css" rel="stylesheet" />
      <script type="text/javascript">
        function openModal() {
            $('#largeModal').modal('show');
        };
          </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" runat="server" id="codAnamnese" />
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ANAMNESES</h2>
            </div>
            <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cadastrar Novo Modelo de Anamnese
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                       <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">NOVO MODELO DE ANAMNESES</button> 
                                     </div>                                   
                                </div>  
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           <p id="mensagemUsuario" runat="server"></p> 
                                        </div>
                                    </div>
                                </div>                          
                        </div>
                    </div>
                </div>
            </div><!--Fim da Linha -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Anamneses Cadastradas
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>                                    
                                </li>
                            </ul>
                        </div>
                       <div class="body table-responsive">
                           <asp:GridView runat="server" ID="gdvAnamneses" AllowPaging="true" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None"
                               PageSize="10" OnPageIndexChanging="gdvAnamneses_PageIndexChanging">    
                               <Columns>
                                   <asp:BoundField DataField="Cod" HeaderText="#" />
                                    <asp:BoundField DataField="Nome" HeaderText="Descrição" />
                                   <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnExcluir" CssClass="btn bg-red waves-effect" OnClientClick="return confirm(&quot;Confirma a exclusão da anamnese?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnExcluir_Command" formnovalidate="formnovalidate" >
                                            </asp:Button>
                                            <asp:Button ID="btnEditar" runat="server" CausesValidation="false" CssClass="btn bg-blue waves-effect" Text="Editar" CommandArgument='<%# Eval("Cod") %>'
                                                  formnovalidate="formnovalidate" OnCommand="btnEditar_Command">
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>      
                               </Columns>
                           </asp:GridView>
                       </div>
                    </div>
               </div>
            </div>
         </div>
    </section>
    <!-- Large Size MODAL --> 
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Modelo de Anamnese</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="txtNomeAnamnese">Nome da Anamnese*</label>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNomeAnamnese" required="required"/>                                                
                                            </div>
                                     </div>
                                </div>
                            </div> <!-- Fim da Linha-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <label>Perguntas para o modelo de anamnese</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <asp:GridView runat="server" ID="gdvPerguntas" AutoGenerateColumns="false" GridLines="None" PageSize="20" OnPageIndexChanging="gdvPerguntas_PageIndexChanging" AllowPaging="true" >
                                   <Columns>
                                     <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <%# Eval("Descricao") + "<br/>" + Eval("LastName")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                    <%--//PREENCHER O GRID--%>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" ID="btnCadastrar" OnClick="btnCadastrar_Click" CssClass="btn bg-green waves-effect" runat="server" />
                            <asp:Button Text="SALVAR" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" Visible="false" Enabled="false" />                            
                             <asp:Button Text="FECHAR" CssClass="btn btn-link waves-effect" runat="server" ID="btnFechar" data-dismiss="modal" OnClick="btnFechar_Click" />                          
                        </div>
                     </div>
                </div>
            </div>
    

         <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
      <!-- Input Mask Plugin Js -->
    <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
     <script src="js/pages/forms/advanced-form-elements.js"></script>
    
    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>
       <script src="js/pages/cards/basic.js"></script>
    <!-- Wait Me Plugin Js -->
    <script src="../../../plugins/waitme/waitMe.js"></script>
</asp:Content>
