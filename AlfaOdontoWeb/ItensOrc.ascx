﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItensOrc.ascx.cs" Inherits="AlfaOdontoWeb.ItensOrc" %>
 <div class="row clearfix">
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                       <label for="txtProcedimento">Procedimento</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="text" id="txtProcedimento" class="form-control" runat="server" required />
                                              </div>
                                          </div>
                                    </div>
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label for="txtDenteRegiao">Dente/Região</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="number" id="txtDenteRegiao" class="form-control" runat="server" required />
                                              </div>
                                          </div>
                                    </div>
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                       <label for="txtValor">Valor</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                                  <input type="text" id="txtValor" class="form-control" runat="server" required />
                                              </div>
                                              <asp:RequiredFieldValidator ID="rfvValor" runat="server" ErrorMessage="Preencha este campo." CssClass="alert-warning"
                                                      ControlToValidate="txtValor" ValidationGroup="itens"></asp:RequiredFieldValidator>
                                          </div>
                                    </div>                                    
                              
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">                                       
        <div class="form-group">  <br />                          
               <asp:Button Text="Adicionar" runat="server" ID="btnAdd" CssClass="btn bg-green waves-effect" OnClick="btnAdd_Click" formnovalidate="formnovalidate" ValidationGroup="itens" />                                             
         </div>
        </div>
     </div>
    <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <fieldset>
    <legend>Itens do Orçamento</legend>
      <asp:GridView runat="server" ID="gdvItens" AutoGenerateColumns="false" CssClass="table table-striped" GridLines="None">
       <Columns>
           <asp:BoundField DataField="ProcedimentoNome" HeaderText="Descrição" />
           <asp:BoundField DataField="DenteRegiao" HeaderText="Dente/Região" />
           <asp:BoundField DataField="ItemValor" HeaderText="Valor" />
           <asp:TemplateField ShowHeader="False">       
               <ItemTemplate>                                                  
               <asp:Button ID="btnItemExcluir" OnClientClick="return confirm(&quot;Confirma a exclusão do item?&quot;);"
               runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("OrcamentoCod") %>' OnCommand="btnItemExcluir_Command" formnovalidate="formnovalidate" 
               CssClass="btn bg-red waves-effect" >
               </asp:Button>
               </ItemTemplate>
               </asp:TemplateField>                          
        </Columns>       
    </asp:GridView>
</fieldset>
 </div>         
</div> <!-- Fim da Linha Grid -->