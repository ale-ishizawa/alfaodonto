﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Anamneses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //AnamnesesCadastradas(); 
            if (!IsPostBack)
            {
                CarregarGrid(0);
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            AnamneseController cAna = new AnamneseController();
            AnamneseViewModel vmAna = new AnamneseViewModel
            {
                Cod = 0,
                Nome = txtNomeAnamnese.Value
            };

            int codRetorno = cAna.Gravar(vmAna);
            if(codRetorno > 0)
            {
                //SUCESSO
                //txtNomeAnamnese.Value = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);                

            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    //PREENCHA O CAMPO NOME DA ANAMNESE
                }
            }

        }

        //private string AnamnesesCadastradas()
        //private void AnamnesesCadastradas()
        //{
        //    List<AnamneseViewModel> _anamneses = new AnamneseController().ObterAnamneses();
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine("<div class='row clearfix'>");
        //    foreach (AnamneseViewModel a in _anamneses)
        //    {
        //        sb.AppendLine("<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>");
        //        sb.AppendLine("<div class='card'>");
        //        sb.AppendLine("<div class='header'>");
        //        sb.AppendLine("<h2>" + a.Nome + "</h2>");
        //        sb.AppendLine("</div>");
        //        sb.AppendLine("<div class='body'>");
        //        sb.AppendLine("<asp:Button ID='btnExcluirAnamnese"+a.Cod+"' runat='server' Text='EXCLUIR' CommandArgument='<%= "+a.Cod+ " %>' OnCommand='btnExcluirAnamnese_Command' CssClass='btn btn-link waves-effect'/>");
        //        sb.AppendLine("<asp:Button ID='btnEditarAnamnese" + a.Cod + "' runat='server' Text='EDITAR' CommandArgument='<%= " + a.Cod + " %>' OnCommand='btnEditarAnamnese_Command' CssClass='btn btn-link waves-effect'/>");
        //        sb.AppendLine("</div> </div> </div>");                
        //    }
        //    sb.AppendLine("</div>");
        //    literalAnamneses.Text = sb.ToString();

        //    //<div class="row clearfix">
        //    //            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        //    //            <div class="card">
        //    //                <div class="header">
        //    //                    <h2>
        //    //                        Basic Card Title<small> Description text here...</small>
        //    //                    </h2>                            
        //    //                </div>
        //    //                <div class="body">
        //    //                    Quis pharetra a pharetra fames blandit.Risus faucibus velit Risus imperdiet mattis neque volutpat, etiam lacinia netus dictum magnis per facilisi sociosqu. Volutpat.Ridiculus nostra.
        //    //                </div>
        //    //            </div>
        //    //            </div>
        //    //         </div>        
        //}

        protected void gdvPerguntas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarGrid(e.NewPageIndex);
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            AnamneseController cAna = new AnamneseController();
            int codRetorno = cAna.Excluir(cod);
            string msg = "";
            switch (codRetorno)
            {
                case 1:
                    msg = "Anamnese excluída com sucesso.";
                    break;
                case -1:
                    msg = "Não foi possível excluir a anamnese. Contate o administrador do sistema.";
                    break;
                case -5:
                    msg = "A anamnese não pode ser excluída pois a mesma possui respostas de pacientes.";
                    break;
                case -10:
                    msg = "Selecione uma anamnese para ser excluída.";
                    break;
            }
            MensagemJS(msg);
            CarregarGrid(0);
        }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void btnEditar_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            AnamneseViewModel vmAna = new AnamneseController().ObterPorCod(cod);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            txtNomeAnamnese.Value = vmAna.Nome;
            codAnamnese.Value = vmAna.Cod.ToString();
            AtivarBotaoSalvar();
            
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            string msg = "";
            AnamneseController cAna = new AnamneseController();
            AnamneseViewModel vmAna = new AnamneseViewModel
            {
                Cod = Convert.ToInt32(codAnamnese.Value),
                Nome = txtNomeAnamnese.Value
            };

            int codRetorno = cAna.Editar(vmAna);

            switch (codRetorno)
            {
                case 1:
                    msg = "Anamnese editada com sucesso.";
                    break;
                case -1:
                    msg = "Erro na aplicação, consulte o administrador do sistema.";
                    break;
                case -10:
                    msg = "Preencha os campos obrigatórios.";
                    break;                
            }
            AtivarBotaoCadastrar();
            MensagemJS(msg);
            codAnamnese.Value = "";
            CarregarGrid(0);
            txtNomeAnamnese.Value = "";
        }

        public void CarregarGrid(int pageIndex)
        {
            gdvAnamneses.PageIndex = pageIndex;
            gdvAnamneses.DataSource = new AnamneseController().ObterAnamneses();
            gdvAnamneses.DataBind();
        }
        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }

        protected void gdvAnamneses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}