﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="ContasReceber.aspx.cs" Inherits="AlfaOdontoWeb.ContasReceber" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>CONTAS A RECEBER</h2>
            </div>    
            <!-- #END# Inline Layout | With Floating Label -->
            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Contas a Receber
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body table-responsive">
                            <asp:GridView runat="server" CssClass="table table-striped" AutoGenerateColumns="False" ID="gdvContasReceber" GridLines="None" PageSize="10"
                                 OnPageIndexChanging="gdvContasReceber_PageIndexChanging" AllowPaging="true" >
                                <Columns>
                                    <asp:BoundField DataField="Cod" HeaderText="#" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="PacienteNome" HeaderText="Paciente" />
                                    <asp:BoundField DataField="FormaPagamento" HeaderText="Forma de Pagamento" />
                                    <asp:BoundField DataField="Valor" HeaderText="Valor" />
                                    <asp:BoundField DataField="DtVencto" HeaderText="Data de Vencimento" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnBaixar" CssClass="btn bg-green waves-effect" OnClientClick="return confirm(&quot;Confirma o pagamento da conta?&quot;);"
                                                runat="server" CausesValidation="false" Text="Excluir" CommandArgument='<%# Eval("Cod") %>' OnCommand="btnBaixar_Command" >
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>                          
                                </Columns>                                                               
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
            </div>
        </section>

</asp:Content>
