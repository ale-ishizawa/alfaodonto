﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Dentistas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        [WebMethod]
        public static string[] PesquisarDentista(string nome)
        {
            string[] dentistas = new DentistaController().Pesquisar(nome);
            return dentistas.ToArray();
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            int ativo = 0;
            DentistaController cDen = new DentistaController();
            if (ckbAtivo.Checked == true)
                ativo = 1;
            else
                ativo = 2;

            DentistaViewModel vmDen = new DentistaViewModel
            {
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Comissao = Convert.ToInt32(txtComissao.Value),
                Complemento = txtComplemento.Value,
                Cpf = txtCpf.Value,
                Cro = txtCro.Value,
                DtNascto = Convert.ToDateTime(txtDtNascto.Value),
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedItem.Text,
                Logradouro = txtLogradouro.Value,
                Nome = txtDentista.Value,
                Numero = txtNumero.Value,
                Rg = txtRg.Value,
                SiglaConselho = txtSiglaConselho.Value,
                TelCelular = txtCelular.Value,
                TelResidencia = txtTelefoneResidencia.Value,
                TempoConsulta = Convert.ToInt32(ddlTempoConsulta.SelectedValue),
                UfConselho = txtUfConselho.Value,
                Ativo = ativo,
                Login = txtLogin.Value,
                Senha = txtSenha.Value                
            };

            int codRetorno = cDen.Gravar(vmDen);
            if(codRetorno > 0)
            {
                //SUCESSO
                MensagemJS("Cadastrado com sucesso.");
            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    //PREENCHA OS Campso obrigatórios
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if (codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação, contate o administrador do sistema.");
                }
            }
        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
            AtivarBotaoCadastrar();
            LimparCampos();
            codDentista.Value = "";
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            DentistaController cDen = new DentistaController();
            DentistaViewModel vmDen = new DentistaViewModel
            {
                Bairro = txtBairro.Value,
                Cep = txtCep.Value,
                Cidade = txtCidade.Value,
                ClinicaCod = 1,
                Cod = Convert.ToInt32(codDentista),
                Comissao = Convert.ToInt32(txtComissao.Value),
                Complemento = txtComplemento.Value,
                Cpf = txtCpf.Value,
                Cro = txtCro.Value,
                DtNascto = Convert.ToDateTime(txtDtNascto.Value),
                Email = txtEmail.Value,
                Estado = ddlEstados.SelectedValue,
                Logradouro = txtLogradouro.Value,
                Nome = txtNome.Value,
                Numero = txtNumero.Value,
                Rg = txtRg.Value,
                SiglaConselho = txtSiglaConselho.Value,
                TelCelular = txtCelular.Value,
                TelResidencia = txtTelefoneResidencia.Value,
                TempoConsulta = Convert.ToInt32(ddlTempoConsulta.SelectedValue),
                UfConselho = txtUfConselho.Value
            };
            int codRetorno = cDen.Editar(vmDen);
            if(codRetorno > 0)
            {
                MensagemJS("O dentista foi editado com sucesso.");
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Preencha os campos obrigatórios.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação, consulte o administrador do sistema.");
                }
            }
            AtivarBotaoCadastrar();
            codDentista.Value = "";
            LimparCampos();

        }

        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void LimparCampos()
        {
            txtBairro.Value = ""; txtCelular.Value = ""; txtCep.Value = "";
            txtCidade.Value = ""; txtComissao.Value = ""; txtComplemento.Value = "";
            txtCpf.Value = ""; txtCro.Value = ""; txtDentista.Value = "";
            txtDtNascto.Value = ""; txtEmail.Value = ""; txtLogin.Value = "";
            txtLogradouro.Value = ""; txtNome.Value = ""; txtNumero.Value = "";
            txtOrgaoEmissor.Value = ""; txtRg.Value = ""; txtSenha.Value = "";
            txtSiglaConselho.Value = ""; txtTelefoneComercial.Value = "";
            txtTelefoneResidencia.Value = ""; txtUfConselho.Value = "";
            ddlEstados.SelectedIndex = 1; ddlTempoConsulta.SelectedIndex = 1;
            ckbAtivo.Checked = true;

        }
    }
}