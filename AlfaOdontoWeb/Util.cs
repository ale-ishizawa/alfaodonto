﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlfaOdontoWeb
{
    internal class Util
    {
        internal static void VerificarAutenticacao()
        {
            if(HttpContext.Current.Session["Cod"] == null || HttpContext.Current.Session["autenticado"] == null ||
               HttpContext.Current.Session["autenticado"].ToString() != "OK")
            {
                HttpContext.Current.Response.Redirect("sair.aspx");
            }
        }
    }
}