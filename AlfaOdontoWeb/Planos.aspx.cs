﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Planos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            PlanoController cPlan = new PlanoController();
            PlanoViewModel vmPlan = new PlanoViewModel
            {
                Cod = 0,
                Descricao = txtNomePlano.Value,
                Padrao = ckbPadrao.Value,
                Valor = Convert.ToDecimal(txtValor.Value)
            };

            int codRetorno = cPlan.Gravar(vmPlan);
            if(codRetorno > 0)
            {
                //SUCESSO
            }
            else
            {
                //ERRO
                if(codRetorno == 10)
                {
                    //Preencha os campos obrigatórios
                }
            }
            
        }
    }
}