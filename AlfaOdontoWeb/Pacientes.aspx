﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Pacientes.aspx.cs" Inherits="AlfaOdontoWeb.Pacientes" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
     <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openModal() {
            $('#largeModal').modal('show');
        };
        function closeModal() {
            $('#largeModal').modal('close');
        };
        
        $(function () {
        $("[id$=txtNome]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Pacientes.aspx/PesquisarPaciente") %>',
                    data: "{ 'nome': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],  
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=codPaciente]").val(i.item.val);
                //openModal();
                var codParam = $("[id$=codPaciente]").val(i.item.val);
                document.location.href = "Sobre.aspx?cod=" + codParam;
            },
            minLength: 1
        });
    });

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>FICHA DO PACIENTE</h2>
            </div>
    <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Pesquisar Paciente ou Cadastrar Novo
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body blue">                            
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNome"/>
                                                <label class="form-label">Buscar Paciente</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                                        <br />OU                                  
                                    </div>                                   
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br />
                                        <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">CADASTRAR NOVO PACIENTE</button>    
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" id="divLabel1" visible="false">
                                        <div class="" runat="server" id="divAlert">
                                           A especialidade foi cadastrada com sucesso.
                                        </div>
                                    </div>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    <!-- Large Size MODAL -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <input type="hidden" id="codPaciente" runat="server" />
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Dados do Paciente</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <label for="txtPaciente">Nome*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtPaciente" required="required"/>                                                
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <label for="radio_3">Sexo</label>
                                        <div class="form-line">                                        
                                        <input name="groupGenero" type="radio" class="with-gap" id="radio_masculino" runat="server" />
                                        <label for="radio_masculino">Masculino</label>
                                        <input name="groupGenero" type="radio" id="radio_feminino" class="with-gap" runat="server" />
                                        <label for="radio_feminino">Feminino</label>
                                        </div>
                                     </div>                                   
                            </div> 
                            <div class="row clearfix">     
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtDtNascto">Data de Nascimento*</label>
                                       <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" class="form-control date" runat="server" id="txtDtNascto" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtCpf">CPF*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control cpf" runat="server" id="txtCpf" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtRg">RG*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control rg" runat="server" id="txtRg" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtOrgaoEmissor">Órgão Emissor*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtOrgaoEmissor" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                            </div>
                            <div class="row clearfix">   
                                 <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                     <label for="txtCelular">Celular*</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control mobile-phone-number" runat="server" id="txtCelular" required="required"/>                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="txtTelefoneResidencia">Telefone Residencial</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control phone-number" runat="server" id="txtTelefoneResidencia" />                                                
                                            </div>
                                        </div>
                                 </div>
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                     <label for="txtTelefoneComercial">Telefone Comercial</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control phone-number" runat="server" id="txtTelefoneComercial" />                                                
                                            </div>
                                        </div>
                                   </div>  
                                <div class="col-lg-3 col-md-3 col-sm3 col-xs-3">
                                    <label for="ddlEstadoCivil">Estado Civil</label>
                                        <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:DropDownList runat="server" ID="ddlEstadoCivil" CssClass="form-control show-tick">
                                                            <asp:ListItem Text="--Estado Civil--" Value="0"/>
                                                            <asp:ListItem Text="Solteiro(a)" Value="1"/>
                                                            <asp:ListItem Text="Casado(a)" Value="2"/>
                                                            <asp:ListItem Text="Divorciado(a)" Value="3"/>
                                                            <asp:ListItem Text="Viúvo(a)" Value="4"/>
                                                        </asp:DropDownList>                                                        
                                       </div>
                                    </div>
                                 </div>                                  
                            </div> <!-- End Row -->                            
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">INFORMAÇÕES ADICIONAIS</a></li>
                                        <li role="presentation"><a href="#profile_animation_1" data-toggle="tab">PLANO</a></li>
                                        <li role="presentation"><a href="#settings_animation_1" data-toggle="tab">ENDEREÇO</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab">DADOS DE ACESSO</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                          <div class="row clearfix">
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                 <label for="txtTelefoneRecado">Telefone Recado*</label>
                                                <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtTelefoneRecado" required="required"/>                                                    
                                                </div>
                                              </div>
                                             </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtNomeRecado" required="required"/>
                                                    <label class="form-label">Nome Recado*</label>
                                                </div>
                                              </div>
                                             </div>
                                           </div>
                                          <div class="row clearfix">
                                             <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtEmail" required="required"/>
                                                        <label class="form-label">Email*</label>
                                                     </div>
                                                 </div>                                                 
                                              </div>
                                          </div>                                                 
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                            <div class="row clearfix">
                                             <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <asp:DropDownList runat="server" ID="ddlPlanos" CssClass="form-control show-tick">
                                                            <asp:ListItem Text="--Plano--" Value="0"/>
                                                        </asp:DropDownList>
                                                        <label class="form-label">Plano</label>
                                                     </div>
                                                  </div>                                                 
                                              </div>    
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtDiaVencto" required="required"/>
                                                        <label class="form-label">Dia do Vencimento</label>
                                                     </div>
                                                 </div>                                                 
                                             </div>      
                                          </div> <!-- End Row-->    
                                        </div> 
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                          <div class="row clearfix">
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtCep" required="required"/>
                                                        <label class="form-label">CEP*</label>
                                                     </div>
                                                 </div>                                                 
                                              </div>    
                                              <div class="col-lg-8 col-md-8 col-sm8 col-xs-8">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtLogradouro" required="required"/>
                                                        <label class="form-label">Logradouro</label>
                                                     </div>
                                                 </div>                                                 
                                             </div>      
                                          </div> <!-- End Row-->  
                                          <div class="row clearfix">
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtNumero" required="required"/>
                                                        <label class="form-label">Número</label>
                                                     </div>
                                                 </div>                                                 
                                             </div> 
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtComplemento" required="required"/>
                                                        <label class="form-label">Complemento</label>
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtBairro" required="required"/>
                                                        <label class="form-label">Bairro</label>
                                                     </div>
                                                 </div>                                                 
                                             </div>   
                                          </div> <!-- End Row -->
                                          <div class="row clearfix">
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">
                                                  <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" runat="server" id="txtCidade" required="required"/>
                                                        <label class="form-label">Cidade</label>
                                                     </div>
                                                 </div>                                                 
                                             </div>  
                                              <div class="col-lg-6 col-md-6 col-sm6 col-xs-6">                                                   
                                                  <asp:DropDownList runat="server" ID="ddlEstados" CssClass="form-control show-tick">
                                                      <asp:ListItem Text="--Estado--" Value="0" />                                                      
                                                  </asp:DropDownList>                                                                            
                                             </div>  
                                          </div>
                                       </div> 
                                       <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">                                           
                                          <div class="row clearfix">                                            
                                             <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtLogin"/>                                   
                                                    <label class="form-label">Login*</label>
                                                </div>
                                              </div>
                                             </div>
                                            <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" runat="server" id="txtSenha" required="required"/>
                                                    <label class="form-label">Senha*</label>
                                                </div>
                                              </div>
                                             </div>
                                              <div class="col-lg-4 col-md-4 col-sm4 col-xs-4">
                                                <div class="form-group form-float">
                                                 <br />
                                                    <div class="switch panel-switch-btn">
                                                        <span class="m-r-10 font-12">Ativo?</span>
                                                        <label>Não<input type="checkbox" id="ckbAtivo" checked="checked" runat="server"/><span class="lever switch-col-cyan"></span>Sim</label>
                                                    </div>                                                    
                                              </div>
                                             </div>
                                           </div>  <!-- End Row -->                                               
                                        </div>          
                                    </div>
                                </div>                                
                            </div> <!-- #END# Tabs With Custom Animations -->
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="CADASTRAR" CssClass="btn bg-green waves-effect" runat="server" ID="btnCadastrar" OnClick="btnCadastrar_Click" />
                            <asp:Button Text="SALVAR" CssClass="btn bg-green waves-effect" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" />
                            <asp:Button Text="FECHAR" runat="server" ID="btnFechar" CssClass="btn btn-link" OnClick="btnFechar_Click" formnovalidate="formnovalidate"/>
                        </div>
                    </div>
                </div>
            </div>
       <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
      <!-- Input Mask Plugin Js -->
    <%--<script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>--%>
     <%--<script src="js/pages/forms/advanced-form-elements.js"></script>--%>
    
    <!-- Multi Select Plugin Js -->
    <%--<script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>--%>

      <!-- Jquery Spinner Plugin Js -->
    <%--<script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>--%>

    <!-- Bootstrap Tags Input Plugin Js -->
    <%--<script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>--%>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>


</asp:Content>
