﻿using AldaOdonto.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class ContasReceber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }
        }

        protected void gdvContasReceber_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarGridContas(e.NewPageIndex);
        }

        protected void btnBaixar_Command(object sender, CommandEventArgs e)
        {
            string msg = "";
            int cod = Convert.ToInt32(e.CommandArgument);
            int codRetorno = new ContaReceberController().BaixarContaReceber(cod);
            if (codRetorno > 0)
            {
                msg = "Conta baixada com sucesso.";
            }
            else
            {
                if (codRetorno == -10)
                {
                    msg = "Selecione a conta a ser baixada.";
                }
                if (codRetorno == -5)
                {
                    msg = "Conta não encontrada na base de dados.";
                }
                if (codRetorno == -1)
                {
                    msg = "Erro na aplicação, contate o administrador do sistema.";
                }
            }
            MensagemJS(msg);
            CarregarGridContas(0);
        }

        protected void CarregarGridContas(int pageIndex)
        {
            gdvContasReceber.PageIndex = pageIndex;
            gdvContasReceber.DataSource = new ContaReceberController().ObterContas();
            gdvContasReceber.DataBind();
        }

        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }
    }
}