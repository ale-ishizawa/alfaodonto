﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Especialidades : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarGridEspecialidades(0);
            }              
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            EspecialidadeController cEsp = new EspecialidadeController();
            EspecialidadeViewModel esp = new EspecialidadeViewModel();
            esp.Descricao = txtDescricao.Value;

            if(cEsp.Gravar(esp) > 0)
            {
                //GRAVOU
                txtDescricao.Value = "";
                divLabel1.Visible = true;
                mensagemUsuario.InnerText = "A especialidade foi cadastrada com sucesso.";
                divAlert.Attributes.Add("class", "alert bg-green");
                CarregarGridEspecialidades(0);
            }
            else
            {
                //ERRO
                txtDescricao.Value = "";
                divLabel1.Visible = true;
                mensagemUsuario.InnerText = "Erro! Verifique se o campo foi preenchido corretamente.";
                divAlert.Attributes.Add("class", "alert bg-red");
                CarregarGridEspecialidades(0);
            }
        }

        public void CarregarGridEspecialidades(int pageIndex)
        {
            gdvEspecialidades.PageIndex = pageIndex;
            gdvEspecialidades.DataSource = new EspecialidadeController().ObterEspecialidades();
            gdvEspecialidades.DataBind();
        }
        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void gdvEspecialidades_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarGridEspecialidades(e.NewPageIndex);
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            EspecialidadeController cEsp = new EspecialidadeController();
            int retorno = cEsp.Excluir(cod);
            string msg = "";
            switch (retorno)
            {
                case 1:
                    msg = "A especialidade foi excluída com sucesso.";
                    CarregarGridEspecialidades(0);
                    break;
                case -1:
                    msg = "Não foi possível excluir a especialidade selecionada.";
                    break;
            }
            MensagemJS(msg);
        }

    }
}