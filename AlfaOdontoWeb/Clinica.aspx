﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Clinica.aspx.cs" Inherits="AlfaOdontoWeb.Clinica" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>CLÍNICA</h2>
            </div>
            <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Dados da Clínica
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body">                            
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNomeClinica" required="required"/>
                                                <label class="form-label">Nome da Clínica*</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtCnpj" required="required"/>
                                                <label class="form-label">CNPJ*</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtIE" required="required"/>
                                                <label class="form-label">Inscrição Estadual*</label>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div> <!-- Fim da Linha-->
                                 <div class="row clearfix">                                                                     
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtTelefone" required="required"/>
                                                <label class="form-label">Telefone*</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtCelular" required="required"/>
                                                <label class="form-label">Celular</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtFax" required="required"/>
                                                <label class="form-label">Fax</label>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div>  <!-- Fim da Linha -->
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtCep" required="required"/>
                                                <label class="form-label">CEP*</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtLogradouro" required="required"/>
                                                <label class="form-label">Logradouro*</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtNumero" required="required"/>
                                                <label class="form-label">Número*</label>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div>  <!-- Fim da Linha --> 
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtComplemento"/>
                                                <label class="form-label">Complemento</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtBairro" required="required"/>
                                                <label class="form-label">Bairro*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtCidade" required="required"/>
                                                <label class="form-label">Cidade*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtEstado" required="required"/>
                                                <label class="form-label">Estado*</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  <!-- Fim da Linha -->
                                <div class="row clearfix">                                                                     
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtEmail" required="required"/>
                                                <label class="form-label">Email*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" runat="server" id="txtSite" required="required"/>
                                                <label class="form-label">Site</label>
                                            </div>
                                        </div>
                                    </div>
                                  </div> 
                            <div class="text-center">
                                <asp:Button Text="SALVAR" ID="btnSalvar" OnClick="btnSalvar_Click" CssClass="btn bg-green waves-effect" runat="server" />             
                            <asp:Button Text="EDITAR" ID="btnEditar" OnClick="btnEditar_Click" CssClass="btn btn-blue waves-effect" runat="server" />                              </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>
    
      <!-- Input Mask Plugin Js -->
    <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
     <script src="js/pages/forms/advanced-form-elements.js"></script>
    
    <!-- Multi Select Plugin Js -->
    <script src="../../plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
    <script src="../../plugins/jquery-spinner/js/jquery.spinner.js"></script>

    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="../../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    
    <!-- noUISlider Plugin Js -->
    <script src="../../plugins/nouislider/nouislider.js"></script>

</asp:Content>
