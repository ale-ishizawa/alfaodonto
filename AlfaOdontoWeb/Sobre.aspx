﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Padrao.Master" AutoEventWireup="true" CodeBehind="Sobre.aspx.cs" Inherits="AlfaOdontoWeb.Sobre" %>

<%@ Register Src="~/UserControlPaciente.ascx" TagPrefix="uc1" TagName="UserControlPaciente" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UserControlPaciente runat="server" ID="UserControlPaciente" />
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               <asp:Literal Text="" runat="server" ID="ltlNomePaciente" />
                                <small></small>
                            </h2>                            
                        </div>
                        <div class="body blue">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <table class="table table-responsive">
                                        <tr>
                                            <td>Celular</td>
                                            <td><asp:Literal Text="" ID="ltlCelular" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Número do Paciente</td>
                                            <td><asp:Literal Text="" ID="ltlNumeroPaciente" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>CPF do Paciente</td>
                                            <td><asp:Literal Text="" ID="ltlCpf" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>RG</td>
                                            <td><asp:Literal Text="" ID="ltlRg" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Idade</td>
                                            <td><asp:Literal Text="" ID="ltlIdade" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Data de Nascimento</td>
                                            <td><asp:Literal Text="" ID="ltlDataNascto" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Sexo</td>
                                            <td><asp:Literal Text="" ID="ltlSexo" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Plano</td>
                                            <td><asp:Literal Text="" ID="ltlPlano" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Endereço</td>
                                            <td><asp:Literal Text="" ID="ltlEndereco" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Bairro</td>
                                            <td><asp:Literal Text="" ID="ltlBairro" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>CEP</td>
                                            <td><asp:Literal Text="" ID="ltlCep" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>Cidade</td>
                                            <td><asp:Literal Text="" ID="ltlCidade" runat="server" /> </td>
                                        </tr>
                                        <tr>
                                            <td>UF</td>
                                            <td><asp:Literal Text="" ID="ltlUf" runat="server" /> </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>    
                        </div>
                        </div>
                    </div>
              </div>
            </div>      
       </section>
     <script src="plugins/jquery/jquery.min.js"></script>
</asp:Content>


