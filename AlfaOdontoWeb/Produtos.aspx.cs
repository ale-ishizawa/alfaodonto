﻿using AldaOdonto.Controller;
using AldaOdonto.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlfaOdontoWeb
{
    public partial class Produtos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LimparAlerta();
            if (!IsPostBack)
            {
                CarregarGridProdutos(0);
            }
        }

        public void CarregarGridProdutos(int pageIndex)
        {
            gdvProdutos.PageIndex = pageIndex;
            gdvProdutos.DataSource = new ProdutoController().ObterProdutos();
            gdvProdutos.DataBind();
        }
        protected void MensagemJS(string mensagem)
        {
            string script = "alert('" + mensagem + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "mensagemAlerta", script, true);
        }

        protected void LimparAlerta()
        {
            divLabel1.Visible = false;
            divAlert.Attributes.Remove("class");
            mensagemUsuario.InnerText = "";
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            ProdutoController cPro = new ProdutoController();
            ProdutoViewModel vmPro = new ProdutoViewModel
            {
                Cod = 0,
                Descricao = txtDescricao.Value,
                Quantidade = Convert.ToInt32(txtQtd.Value),
                QtdCritica = Convert.ToInt32(txtQtdCritica.Value)
            };

            int codRetorno = cPro.Gravar(vmPro);
            if(codRetorno > 0)
            {
                //SUCESSO
                LimparCampos();
                divLabel1.Visible = true;
                mensagemUsuario.InnerText = "O produto foi cadastrado com sucesso.";
                divAlert.Attributes.Add("class", "alert bg-green");
                CarregarGridProdutos(0);
            }
            else
            {
                //ERRO
                if(codRetorno == -10)
                {
                    
                    divLabel1.Visible = true;
                    mensagemUsuario.InnerText = "Erros de Validação! Verifique se os campos foram preenchidos corretamente.";
                    divAlert.Attributes.Add("class", "alert bg-red");
                    CarregarGridProdutos(0);
                }
                if(codRetorno == -1)
                {
                    divLabel1.Visible = true;
                    mensagemUsuario.InnerText = "Alerta! Não foi possível cadastrar o produto no momento, consulte o administrador do sistema.";
                    divAlert.Attributes.Add("class", "alert bg-orange");
                    CarregarGridProdutos(0);
                }
            }
        }

        protected void gdvProdutos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CarregarGridProdutos(e.NewPageIndex);
        }

        protected void btnExcluir_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            ProdutoController cPro = new ProdutoController();
            int codRetorno = cPro.Excluir(cod);
            string msg = "";
            switch (codRetorno)
            {
                case 1:
                    msg = "O produto foi excluído com sucesso.";
                    CarregarGridProdutos(0);
                    break;
                case -1:
                    msg = "Não foi possível excluir o produto. Contate o administrador do sistema.";
                    break;
                case -5:
                    msg = "O produto não pode ser excluído pois o mesmo já teve baixa de estoque.";
                    break;
                case -10:
                    msg = "Selecione um produto para ser excluído.";
                    break;
            }
            MensagemJS(msg);
            CarregarGridProdutos(0);
           
        }

        protected void btnEditar_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            codProduto.Value = cod.ToString();
            ProdutoViewModel vmPro = new ProdutoController().ObterPorCod(cod);
            txtDescricao.Value = vmPro.Descricao;
            txtQtd.Value = vmPro.Quantidade.ToString();
            txtQtdCritica.Value = vmPro.QtdCritica.ToString();
            AtivarBotaoSalvar();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            ProdutoController cPro = new ProdutoController();
            ProdutoViewModel vmPro = new ProdutoViewModel
            {
                Cod = Convert.ToInt32(codProduto.Value),
                Descricao = txtDescricao.Value,
                Quantidade = Convert.ToInt32(txtQtd.Value),
                QtdCritica = Convert.ToInt32(txtQtdCritica.Value)
            };
            int codRetorno = cPro.Editar(vmPro);
            if (codRetorno > 0)
            {
                MensagemJS("O produto foi editado com sucesso.");
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Preencha os campos corretamente.");
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Não foi possível editar o produto.");
                }
            }
            AtivarBotaoCadastrar();
            codProduto.Value = "";
            CarregarGridProdutos(0);
            LimparCampos();

        }

        protected void btnFechar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
            AtivarBotaoCadastrar();
            LimparCampos();
            codProduto.Value = "";
        }

        public void AtivarBotaoCadastrar()
        {
            btnCadastrar.Visible = true;
            btnCadastrar.Enabled = true;
            btnSalvar.Visible = false;
            btnSalvar.Enabled = false;
        }

        public void AtivarBotaoSalvar()
        {
            btnSalvar.Visible = true;
            btnSalvar.Enabled = true;
            btnCadastrar.Visible = false;
            btnCadastrar.Enabled = false;
        }

        protected void LimparCampos()
        {
            txtDescricao.Value = "";
            txtQtd.Value = "";
            txtQtdCritica.Value = "";
        }

        protected void btnBaixarEstoque_Command(object sender, CommandEventArgs e)
        {
            int cod = Convert.ToInt32(e.CommandArgument);
            codProduto.Value = cod.ToString();
            ProdutoViewModel vmPro = new ProdutoController().ObterPorCod(cod);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalSmall();", true);
        }

        protected void btnBaixar_Click(object sender, EventArgs e)
        {
            int QtdBaixar = Convert.ToInt32(txtMovimento.Value);
            ProdutoController cPro = new ProdutoController();
            ProdutoViewModel vmPro = new ProdutoViewModel
            {
                Cod = Convert.ToInt32(codProduto.Value),
                Quantidade = QtdBaixar                
            };
            int codRetorno = cPro.BaixarEstoque(vmPro);
            if(codRetorno > 0)
            {
                //SUCESSO
                MensagemJS("A baixa no estoque foi efetuada com sucesso.");
                codProduto.Value = "";
                CarregarGridProdutos(0);
                txtMovimento.Value = "";
            }
            else
            {
                if(codRetorno == -10)
                {
                    MensagemJS("Erro de validação");
                    txtMovimento.Value = "";
                }
                if(codRetorno == -1)
                {
                    MensagemJS("Erro na aplicação.");
                    txtMovimento.Value = "";
                }
            }

        }

        protected void btnFecharBaixaEstoque_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalSmall();", true);
            txtMovimento.Value = "";
            codProduto.Value = "";
        }
    }
}